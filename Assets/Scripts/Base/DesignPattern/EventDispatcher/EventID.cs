public enum EventID
{

    //GamePlaye
    SaveIdFruit,
    IncreseProces,
    LoseGame,
    WinGame,
    CreatePoint,
    StopRun,
    StartSlash,
    CanSlash,
    EndSlash,
    HandOut,

    // Settings
    OnSoundChange,
    OnMusicChange,
    OnVibrationChange,

    //ItemCollection
    OnReloadCollectionScrollView,
    OnReloadItemScrollView,
    OnShowReward,


    //Cheat
    CheatAllItem,
}