﻿
using UnityEngine;
using UI.LoadingScene;
using DataAccount;
using AppsFlyerSDK;

namespace Base.Core
{
    public class GameManager : SingletonMono<GameManager>
    {
        private const int TargetFrameRate = 60;

        protected override void Awake()
        {
            base.Awake();
            AppsFlyer.sendEvent("app_open", null);
            FirebaseManager.Instance.LogAnalyticsEvent("app_open");
            SetForcedFrameRate();
        }

        private void Start()
        {
            SetDefaultCupKnife();
        }

        public void SetForcedFrameRate()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = TargetFrameRate;
        }

        public void LoadScene(SceneName sceneName, bool asyncLoad = true)
        {
            // AdsManager.Instance.HideBannerAds();

            GameManager.Instance.StopAllCoroutines();
            if (asyncLoad)
            {
                LoadingScene.SetSceneName(sceneName);

                UnityEngine.SceneManagement.SceneManager.LoadScene(SceneName.LoadingScene.ToString());
            }
            else
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName.ToString());
            }
        }

        public void LoadSenceNew(SceneName sceneName)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName.ToString());
        }

        private void SetDefaultCupKnife()
        {
            var cup = new PlayerCollectionCup(3001, true);
            var cupList = new PlayerCollectionCupList();
            cupList.Add(cup);
            DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);
            //DataAccountPlayer.PlayerDataAccount.SetCupID(3001, $"Sprite/CupItems/3001");

            var knife = new PlayerCollectionKnife(4001, true);
            var knifeList = new PlayerCollectionKnifeList();
            knifeList.Add(knife);
            DataAccountPlayer.PlayerCollectionData.ClaimKnife(knifeList);
            //DataAccountPlayer.PlayerDataAccount.SetKnifeID(4001,$"Sprite/KnifeItems/4001");
        }

        void OnApplicationQuit()
        {
            Debug.Log("Drop Game " + Time.time + " seconds");
            FirebaseManager.Instance.LogAnalyticsEvent($"drop_level_{DataAccountPlayer.PlayerProcessData.currentLevel}");
            AppsFlyer.sendEvent($"drop_level_{DataAccountPlayer.PlayerProcessData.currentLevel}", null);
        }
    }
}
// văn mẫu nghỉ việc
//Kính gửi: Ban Giám Đốc công ty.

//Tên tôi là: Lê Minh Hiếu.
//Vị trí: Unity Developer - Team Product.
//Do định hướng của bản thân không còn phù hợp với công ty.
//Bên cạnh đó, tôi tự cảm thấy khả năng của bản thân chưa thực sự phù hợp với nhu cầu mà công ty mong muốn. Vì vậy tôi viết mail này xin được nghỉ việc tại công ty.
//Tôi xin chân thành cảm ơn Ban Giám Đốc và mọi người đã cho tôi môi trường thoải mái để tự do phát triển kiến thức và các kỹ năng sống của bản thân trong thời gian vừa qua. Cảm ơn Ban Giám Đốc cũng như đồng nghiệp đã truyền cho tôi những động lực để phấn đấu trong công việc và cuộc sống.
//Chúc cho công ty sẽ đạt được những thành công như mong muốn. Mong rằng sau này sẽ có cơ hội được làm việc cùng công ty trong tương lai.

//Vậy tôi làm đơn này xin được nghỉ việc từ ngày 12/02/2024. Tôi sẽ bàn giao công việc của mình đầy đủ và có trách nhiệm.

//Tôi mong muốn được sự chấp thuận của Ban Giám Đốc.

//Chúc công ty ngày càng thành công và phát triển!