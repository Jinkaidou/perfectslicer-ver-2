using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{

    [Serializable]
    public class PhaseCollectionData
    {
        [GUIColor(0f, 2f, 1f)]
        public int mapId;
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public List<PhaseData> phase1 = new List<PhaseData>();
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public List<PhaseData> phase2 = new List<PhaseData>();
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public Sprite itemReward;
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public int moneyLevel;
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public int moneyLevelReward;
    }
}