using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [CreateAssetMenu(menuName = "Data/ListCollectionData", fileName = "ListCollectionData")]

    [Serializable]
    public class ListCollectionData : ScriptableObject
    {
        public List<CollectionData> collectiveCup = new List<CollectionData>();
        public List<CollectionData> collectiveKnife = new List<CollectionData>();
    }
}