using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [CreateAssetMenu(menuName = "Data/Difficulty", fileName = "DifficultyData")]
    [Serializable]
    public class DataDifficultyCollection : ScriptableObject
    {
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public List<DataDifficulty> data = new List<DataDifficulty>();
    }
}

