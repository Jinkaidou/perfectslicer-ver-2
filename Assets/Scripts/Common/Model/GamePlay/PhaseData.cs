using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [Serializable]
    public class PhaseData
    {
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public int levelDiff;
    }
}