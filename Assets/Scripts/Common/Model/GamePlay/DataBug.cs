using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [Serializable]
    public class DataBug
    {
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public int idBug;
        public float speed;
        public float speedFly;
    }
}