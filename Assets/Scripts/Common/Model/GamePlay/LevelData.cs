using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{

    [CreateAssetMenu(menuName = "Data/Phase", fileName = "PhaseData")]
    public class LevelData : ScriptableObject
    {
        public List<PhaseCollectionData> phaseMain = new List<PhaseCollectionData>();
    }
}