using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [Serializable]
    public class DataDifficulty
    {
        [GUIColor(0f, 3f, 0.5f)]
        public int difficult;
        [GUIColor(0f, 1f, 0.2f)]
        public List<int> fruitId = new List<int>();
        public GameObject fruit;
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public List<DataBug> bug = new List<DataBug>();
    }
}