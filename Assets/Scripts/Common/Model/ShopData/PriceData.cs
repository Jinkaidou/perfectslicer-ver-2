using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [Serializable]
    public class PriceData
    {
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public int levelPress;
        public int price;
    }
}
