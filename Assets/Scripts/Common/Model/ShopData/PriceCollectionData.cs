using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [CreateAssetMenu(menuName = "Data/PriceCollectionData", fileName = "PriceCollectionData")]

    [Serializable]
    public class PriceCollectionData : ScriptableObject
    {
        public int maxPrice;
        public List<PriceData> priceDatas = new List<PriceData>();
    }
}