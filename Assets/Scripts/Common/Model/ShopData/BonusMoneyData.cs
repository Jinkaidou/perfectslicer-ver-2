using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [CreateAssetMenu(menuName = "Data/BonusMoneyData", fileName = "BonusMoneyData")]

    [Serializable]
    public class BonusMoneyData : ScriptableObject
    {
        public int levelPress;
        public int baseMoney;
        public int bonusMoney;
        public int maxMoneyEarn;
    }
}