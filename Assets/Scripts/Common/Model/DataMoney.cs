using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
namespace JinLab.DataBase
{
    [CreateAssetMenu(menuName = "Data/Money", fileName = "MoneyData")]
    [Serializable]
    public class DataMoney : ScriptableObject
    {
        [GUIColor(0.3f, 3f, 0.8f)]
        public int baseMoney;
        [GUIColor(0.3f, 3f, 0.8f)]
        public int levelUP;
        [GUIColor(0.3f, 3f, 0.8f)]
        public int bonus;

        [GUIColor(1f, 1f, 0f)]
        public int baseMoneyAds;
        [GUIColor(1f, 1f, 0f)]
        public int levelUPAds;
        [GUIColor(1f, 1f, 0)]
        public int bonusAds;
    }
}