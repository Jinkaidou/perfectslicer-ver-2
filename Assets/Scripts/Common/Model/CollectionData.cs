using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [Serializable]
    public class CollectionData
    {
        public TypeCose typeUnlock;
        public int Id;
        public string name;
        public float cost;

        public bool HaveIt;
    }
}