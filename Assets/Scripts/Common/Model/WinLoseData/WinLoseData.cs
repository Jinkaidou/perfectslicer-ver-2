using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [CreateAssetMenu(menuName = "Data/WinLoseData", fileName = "WinLoseData")]

    [Serializable]
    public class WinLoseData : ScriptableObject
    {
        public float lose;

        public float oneStar;
        public float twoStar;
        public float threeStar;


    }
}