using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace JinLab.DataBase
{
    [CreateAssetMenu(menuName = "Data/Map", fileName = "MapData")]
    public class MapCollectionData : ScriptableObject
    {
        public List<MapData> mapData = new List<MapData>();
    }
}