namespace JinLab.DataBase
{
    public enum TypeMove
    {
        Static,
        Rotate,
        LeftRight,
        UpDown,
    }
}