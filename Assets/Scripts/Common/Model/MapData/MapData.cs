﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;


namespace JinLab.DataBase
{

    [Serializable]
    public class MapData
    {
        [GUIColor(0f, 2f, 1f)]
        public int mapId;
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public List<int> fruitId = new List<int>();
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public int howManyPiece;
        [GUIColor(0.1f, 0.5f, 0.7f)]
        public TypeMove typeMove;
        [GUIColor(0f, 0.7f, 0.7f)]
        public int howManySlash;

        public int RandomFruit()
        {
            var idIndex = UnityEngine.Random.Range(0,fruitId.Count);
            var idfruit = fruitId[idIndex];
            // lưu data
            return idfruit;
        }

    }
}