using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataAccount;
namespace GamePlay
{
    public class BackGroundController : MonoBehaviour
    {
        public List<GameObject> listBg = new List<GameObject>();

        private void Awake()
        {
            ChangeBackGround();
        }

        private void ChangeBackGround()
        {
            listBg[DataAccountPlayer.PlayerDataAccount.mapId].gameObject.SetActive(true);

            if (DataAccountPlayer.PlayerDataAccount.mapId == 5)
            {
                DataAccountPlayer.PlayerDataAccount.SetMapId(0);
            }
            else if (DataAccountPlayer.PlayerDataAccount.mapId < 6)
            {
                var idMap = DataAccountPlayer.PlayerDataAccount.mapId + 1;
                DataAccountPlayer.PlayerDataAccount.SetMapId(idMap);
            }
        }
    }
}