namespace DataAccount
{
    public struct DataAccountPlayerConstants
    {
        public const string PlayerSettings = "PlayerSettings";
        public const string PlayerTutorialData = "PlayerTutorialData";
        public const string PlayerMoneyData = "PlayerMoneyData";
        public const string PlayerProcessData = "PlayerProcessData";
        public const string PlayerProcessShopData = "PlayerProcessShopData";
        public const string PlayerCollectionData = "PlayerCollectionData";
        public const string PlayerDataAccount = "PlayerDataAccount";
    }
}