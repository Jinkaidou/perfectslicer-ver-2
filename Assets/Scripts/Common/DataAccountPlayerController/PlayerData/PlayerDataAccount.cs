using Base.Core;
using System.Collections;
using System.Collections.Generic;

namespace DataAccount
{
    public class PlayerDataAccount
    {
        public int knifeId;
        public int cupId;
        public int mapId;
        public string pathResourceKnife;
        public string pathResourceCup;

        public List<int> idCupHas = new List<int>();
        public List<int> idKnifeHas = new List<int>();
        public bool firstTimeGoShop;

        public void ChangeShopStatus(bool changeStatus)
        {
            firstTimeGoShop = changeStatus;
            DataAccountPlayer.SavePlayerAccountData();
        }

        public void AddIdCupHas(int id)
        {
            idCupHas.Add(id);
            DataAccountPlayer.SavePlayerAccountData();
        }

        public void RemoveIdCupItem(int id)
        {
            idCupHas.RemoveAt(id);
            DataAccountPlayer.SavePlayerAccountData();
        }

        public void AddIdKnifeHas(int id)
        {
            idKnifeHas.Add(id);
            DataAccountPlayer.SavePlayerAccountData();
        }

        public void RemoveIdKnifeItem(int id)
        {
            idKnifeHas.RemoveAt(id);
            DataAccountPlayer.SavePlayerAccountData();
        }

        public void SetMapId(int id)
        {
            mapId = id;
            DataAccountPlayer.SavePlayerAccountData();
        }

        public void SetKnifeID(int knifeid, string path)
        {
            knifeId = knifeid;
            pathResourceKnife = path;
            DataAccountPlayer.SavePlayerAccountData();
        }

        public void SetCupID(int cupid, string path)
        {
            cupId = cupid;
            pathResourceCup = path;
            DataAccountPlayer.SavePlayerAccountData();
        }

        public string PathKnife()
        {
            if(pathResourceKnife is null)
            {
                return $"Sprite/KnifeItems/4001";
            }
            return pathResourceKnife;
        }

        public string PathCup()
        {
            if (pathResourceCup is null)
            {
                return $"Sprite/CupItems/3001";
            }
            return pathResourceCup;
        }
    }
}