﻿using System.Collections;
using System.Collections.Generic;

namespace DataAccount
{
    public static class DataAccountPlayer
    {
        private static PlayerSettings _playerSettings;
        private static PlayerTutorialData _playerTutorialData;
        private static PlayerMoneyData _playerMoneyData;
        private static PlayerProcessData _playerProcessData;
        private static PlayerProcessShopData _playerProcessShopData;
        private static PlayerCollectionData _playerCollectionData;
        private static PlayerDataAccount _playerDataAccount;

        #region Getters

        public static PlayerDataAccount PlayerDataAccount
        {
            get
            {
                if (_playerDataAccount != null)
                {
                    return _playerDataAccount;
                }

                var PlayerDataAccount = new PlayerDataAccount();
                _playerDataAccount = ES3.Load(DataAccountPlayerConstants.PlayerDataAccount, PlayerDataAccount);
                return _playerDataAccount;
            }
        }

        public static PlayerCollectionData PlayerCollectionData
        {
            get
            {
                if (_playerCollectionData != null)
                {
                    return _playerCollectionData;
                }

                var playerCollectionData = new PlayerCollectionData();
                _playerCollectionData = ES3.Load(DataAccountPlayerConstants.PlayerCollectionData, playerCollectionData);
                return _playerCollectionData;
            }
        }

        public static PlayerSettings PlayerSettings
        {
            get
            {
                if (_playerSettings != null)
                {
                    return _playerSettings;
                }

                var playerSettings = new PlayerSettings();
                _playerSettings = ES3.Load(DataAccountPlayerConstants.PlayerSettings, playerSettings);
                return _playerSettings;
            }
        }


        public static PlayerTutorialData PlayerTutorialData
        {
            get
            {
                if (_playerTutorialData != null)
                    return _playerTutorialData;

                var playerTutorialData = new PlayerTutorialData();
                _playerTutorialData = ES3.Load(DataAccountPlayerConstants.PlayerTutorialData, playerTutorialData);
                return _playerTutorialData;
            }
        }

        public static PlayerMoneyData PlayerMoneyData
        {
            get
            {
                if (_playerMoneyData != null)
                    return _playerMoneyData;

                var playerMoneyData = new PlayerMoneyData();
                _playerMoneyData = ES3.Load(DataAccountPlayerConstants.PlayerMoneyData, playerMoneyData);
                return _playerMoneyData;
            }
        }

        public static PlayerProcessData PlayerProcessData
        {
            get
            {
                if (_playerProcessData != null)
                    return _playerProcessData;

                var playerProcessData = new PlayerProcessData();
                _playerProcessData = ES3.Load(DataAccountPlayerConstants.PlayerProcessData, playerProcessData);
                return _playerProcessData;
            }
        }

        public static PlayerProcessShopData PlayerProcessShopData
        {
            get
            {
                if (_playerProcessShopData != null)
                {
                    return _playerProcessShopData;
                }
                var playerProcessShopData = new PlayerProcessShopData();
                _playerProcessShopData = ES3.Load(DataAccountPlayerConstants.PlayerProcessShopData, playerProcessShopData);
                return _playerProcessShopData;
            }
        }

        #endregion

        #region Save

        public static void SavePlayerSettings()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerSettings, _playerSettings);
        }

        public static void SavePlayerTutorialData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerTutorialData, _playerTutorialData);
        }

        public static void SavePlayerMoneyData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerMoneyData, _playerMoneyData);
        }

        public static void SavePlayerProcessData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerProcessData, _playerProcessData);
        }

        public static void SavePlayerProcessShopData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerProcessShopData, _playerProcessShopData);
        }

        public static void SavePlayerCollectionData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerCollectionData, _playerCollectionData);
        }

        public static void SavePlayerAccountData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerDataAccount, _playerDataAccount);
        }

        #endregion
    }
}

