using Base.Core;

namespace DataAccount
{
    public class PlayerMoneyData
    {
        //baseData only for Init Stage
        public int currentMoneyOnStage;
        public int levelStep;
        public int bonusMoney;

        //baseData only for Init Ads
        public int currentMoneyOnAds;
        public int levelStepAds;
        public int bonusMoneyAds;

        // Data Player
        public int currentmoney;
        public int countLevelStage;
        public int countLevelAds;



        #region GettersData

        // ads Money

        public void InitDataAds(int money, int level, int moneyBonus)
        {
            SetCurrentMoneyOnAds(money);
            SetLevelStepAds(level);
            SetBonusAds(moneyBonus);
            DataAccountPlayer.SavePlayerMoneyData();
        }

        private void SetCurrentMoneyOnAds(int money)
        {
            if (currentMoneyOnAds == 0)
            {
                currentMoneyOnAds = money;
            }
        }
        private void SetLevelStepAds(int level)
        {
            if (levelStepAds == 0)
            {
                levelStepAds = level;
            }
        }
        private void SetBonusAds(int moneyBonus)
        {
            if (bonusMoneyAds == 0)
            {
                bonusMoneyAds = moneyBonus;
            }
        }

        // stage Money
        public void InitDataStage(int money, int level, int moneyBonus)
        {
            SetCurrentMoneyOnStage(money);
            SetLevelStep(level);
            SetBonus(moneyBonus);
            DataAccountPlayer.SavePlayerMoneyData();
        }
        private void SetCurrentMoneyOnStage(int money)
        {
            if (currentMoneyOnStage == 0)
            {
                currentMoneyOnStage = money;
            }
        }
        private void SetLevelStep(int level)
        {
            if (levelStep == 0)
            {
                levelStep = level;
            }
        }
        private void SetBonus(int moneyBonus)
        {
            if (bonusMoney == 0)
            {
                bonusMoney = moneyBonus;
            }
        }

        #endregion

        public void CalculateMoneyProcess()
        {
            countLevelStage += 1;
            countLevelAds += 1;

            if (countLevelStage == levelStep && levelStep != 0)
            {
                currentMoneyOnStage += bonusMoney;
                countLevelStage = 0;
            }
            if (countLevelAds == levelStepAds && levelStepAds != 0)
            {
                currentMoneyOnAds += bonusMoneyAds;
                countLevelAds = 0;
            }
            DataAccountPlayer.SavePlayerMoneyData();
        }

        public void IncreaseMoneyStage()
        {
            if (currentMoneyOnStage != 0)
            {
                currentmoney += currentMoneyOnStage;
                DataAccountPlayer.SavePlayerMoneyData();
            }
        }

        public void IncreaseMoneyAds()
        {
            if (currentMoneyOnAds != 0)
            {
                currentmoney += currentMoneyOnAds;
                DataAccountPlayer.SavePlayerMoneyData();
            }
        }

        public void ReduceMoney(int moneyReduce)
        {
            if (currentmoney > moneyReduce)
            {
                currentmoney -= moneyReduce;
                DataAccountPlayer.SavePlayerMoneyData();
            }
        }

        public void IncreaseMoneyByValue(int moneyIncrease)
        {
            if (moneyIncrease > 0)
            {
                currentmoney += moneyIncrease;
                DataAccountPlayer.SavePlayerMoneyData();
            }
        }

    }
}