using Base.Core;

namespace DataAccount
{
    public class PlayerProcessShopData
    {
        public int currentLevelPurchase;
        public int currentLevelEarnMoney;

        public int moneyEarn;
        public int BonusmoneyEarn;
        public int maxMoneyEarn;

        public void IncreaseLevelEarnMoney()
        {
            currentLevelEarnMoney += 1;
            DataAccountPlayer.SavePlayerProcessShopData();
        }

        public void IncreaseLevelPurchase()
        {
            currentLevelPurchase += 1;
            DataAccountPlayer.SavePlayerProcessShopData();
        }

        public void SetMoneyEarnByAds(int baseMoney, int BonusMoney, int maxMoney)
        {
            if (currentLevelEarnMoney == 0)
            {
                moneyEarn = baseMoney;
                BonusmoneyEarn = BonusMoney;
                maxMoneyEarn = maxMoney;
                DataAccountPlayer.SavePlayerProcessShopData();
            }
        }

        public void IncreasMoneyEarn()
        {
            if(moneyEarn >= maxMoneyEarn)
            {
                moneyEarn = maxMoneyEarn;
                DataAccountPlayer.SavePlayerProcessShopData();
            }
            else
            {
                moneyEarn += BonusmoneyEarn;
                DataAccountPlayer.SavePlayerProcessShopData();
            }
        }

        public void SetMoney()
        {
            moneyEarn += 1000000;
            DataAccountPlayer.SavePlayerProcessShopData();
        }
    }
}