using Base.Core;

namespace DataAccount
{
    public class PlayerProcessData
    {
        public int dayCheckin;
        public int currentLevel = 1;
        public int idCollection;

        public int day = 0;
        public long timeCheckin;

        public void SetLevel(int level)
        {
            if (level != 0)
            {
                currentLevel = level;
                DataAccountPlayer.SavePlayerProcessData();
            }
        }

        public void IncreaseLevel()
        {
            currentLevel += 1;
            DataAccountPlayer.SavePlayerProcessData();
        }

        public void SetDayLimit()
        {
            day += 1;
            DataAccountPlayer.SavePlayerProcessData();
        }

        public void SetTimeCheckin()
        {
            timeCheckin = UtilityGame.GetCurrentTime();
            DataAccountPlayer.SavePlayerProcessData();
        }

        public bool IsDayClaimable()
        {
            if (UtilityGame.GetCurrentTime() - timeCheckin >= 86400)
            {
                return true;
            }
            return false;
        }

    }
}