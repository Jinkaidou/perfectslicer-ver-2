using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controller.LoadData;
using JinLab.DataBase;

namespace DataAccount
{
    public class PlayerCollectionData
    {
        public int page;
        public int selectedCup;
        public int selectedKnife;

        public PlayerCollectionCupList cupData = new PlayerCollectionCupList();
        public PlayerCollectionKnifeList knifeData = new PlayerCollectionKnifeList();

        public void InitCollectionData(int _page, int _selectedCup, int _selectedKnife, PlayerCollectionCupList _cupData, PlayerCollectionKnifeList _knifeData)
        {
            page = _page;
            selectedCup = _selectedCup;
            selectedKnife = _selectedKnife;
            cupData = _cupData;
            knifeData = _knifeData;

            DataAccountPlayer.PlayerCollectionData.ClaimCup(_cupData);

            DataAccountPlayer.PlayerCollectionData.ClaimKnife(_knifeData);

            DataAccountPlayer.SavePlayerCollectionData();
        }

        public void ClaimCup(PlayerCollectionCupList _cupItem)
        {
            for (int i = 0; i < _cupItem.Count; i++)
            {
                var cupItem = cupData.Find(x => x.id == _cupItem[i].id);
                if (cupItem == null)
                {
                    cupData.Add(_cupItem[i]);
                    ListCollectionData data = LoadResourceController.Instance.LoadDataCollection();
                    var cupClaim = data.collectiveCup.Find(x => x.Id == _cupItem[i].id);
                    if (cupClaim.typeUnlock == TypeCose.UnlockShop)
                    {
                        cupData[cupData.Count - 1].isClaimAds = true;
                    }
                }
                else
                {
                    cupItem.isClaimAds = true;
                    selectedCup = _cupItem[i].id;
                }
            }

            DataAccountPlayer.SavePlayerCollectionData();
        }

        public void ClaimKnife(PlayerCollectionKnifeList _knifeItem)
        {
            for (int i = 0; i < _knifeItem.Count; i++)
            {
                var knifeItem = knifeData.Find(x => x.id == _knifeItem[i].id);
                if (knifeItem == null)
                {
                    knifeData.Add(_knifeItem[i]);
                    ListCollectionData data = LoadResourceController.Instance.LoadDataCollection();
                    var knifeClaim = data.collectiveKnife.Find(x => x.Id == _knifeItem[i].id);
                    if (knifeClaim.typeUnlock == TypeCose.UnlockShop)
                    {
                        knifeData[knifeData.Count - 1].isClaimAds = true;
                    }
                }
                else
                {
                    knifeItem.isClaimAds = true;
                    selectedKnife = _knifeItem[i].id;
                }
            }

            DataAccountPlayer.SavePlayerCollectionData();
        }
    }

    [System.Serializable]
    public class PlayerCollectionCupList : List<PlayerCollectionCup>
    {

    }

    [System.Serializable]
    public class PlayerCollectionKnifeList : List<PlayerCollectionKnife>
    {

    }

    [System.Serializable]
    public class PlayerCollectionItem
    {
        public int id;
        public bool isClaimAds;
    }


    [System.Serializable]
    public class PlayerCollectionCup : PlayerCollectionItem
    {
        // public int id;
        // public bool isClaimAds;

        public PlayerCollectionCup(int _id, bool _isClaimAds = false)
        {
            id = _id;
            isClaimAds = _isClaimAds;
        }
    }

    [System.Serializable]
    public class PlayerCollectionKnife : PlayerCollectionItem
    {
        // public int id;
        // public bool isClaimAds;

        public PlayerCollectionKnife(int _id, bool _isClaimAds = false)
        {
            id = _id;
            isClaimAds = _isClaimAds;
        }
    }
}
