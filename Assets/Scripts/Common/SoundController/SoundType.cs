﻿namespace Base.Core.Sound
{
    // DO NOT ADD NEW SOUND IN THE MIDDLE OF ENUM
    public enum SoundType
    {
        knife_slash1,
        knife_slash2,
        knife_slash3,
        bug_cut1,
        bug_cut2,
        juice_collect,
        victory,
        coin_pickup,
        open_gift,
        button_click,
    }
}