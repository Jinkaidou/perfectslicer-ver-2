using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CW.Common;
using System.Text.RegularExpressions;

public class TestPos : MonoBehaviour
{
    public Transform tf_Start;
    public Transform tf_End;
    public Transform look;
    public Transform head;

    public Transform bug1;
    public Transform bug2;

    public virtual void OnEnable()
    {
        CwInputManager.OnFingerDown += HandleFingerDown;
        CwInputManager.OnFingerUp += HandleFingerUp;
        CwInputManager.OnFingerUpdate += HandleFingerUpdate;
    }

    public virtual void OnDisable()
    {
        CwInputManager.OnFingerDown -= HandleFingerDown;
        CwInputManager.OnFingerUp -= HandleFingerUp;
        CwInputManager.OnFingerUpdate -= HandleFingerUpdate;
    }

    public virtual void OnDestroy()
    {
        CwInputManager.OnFingerDown -= HandleFingerDown;
        CwInputManager.OnFingerUp -= HandleFingerUp;
        CwInputManager.OnFingerUpdate -= HandleFingerUpdate;
    }

    public virtual void HandleFingerDown(CwInputManager.Finger finger)
    {
        tf_Start.position = Camera.main.ScreenToWorldPoint(new Vector3(finger.ScreenPosition.x, finger.ScreenPosition.y, Camera.main.nearClipPlane));
    }

    public virtual void HandleFingerUp(CwInputManager.Finger finger)
    {
        tf_End.position = Camera.main.ScreenToWorldPoint(new Vector3(finger.ScreenPosition.x, finger.ScreenPosition.y, Camera.main.nearClipPlane));
        // CheckLeftOrRight(tf_Start.position - );
        look.position = tf_End.position;
        Rotate();
        CheckLeftOrRight(look, bug1);
        CheckLeftOrRight(look, bug2);
    }

    public virtual void HandleFingerUpdate(CwInputManager.Finger finger)
    {

    }

    void CheckLeftOrRight(Transform youTrans, Transform enemyTrans)
    {
        Vector3 youDir = youTrans.right;

        //The direction from you to the waypoint
        Vector3 waypointDir = enemyTrans.position - youTrans.position;

        //The dot product between the vectors
        float dotProduct = Vector3.Dot(youDir, waypointDir);

        //Now we can decide if we should turn left or right
        if (dotProduct > 0f)
        {
            Debug.Log(enemyTrans.name + " on right");
            // right = true;
            // left = false;
        }
        else
        {
            Debug.Log(enemyTrans.name + " on left");
            // right = false;
            // left = true;
        }
    }

    [Sirenix.OdinInspector.Button]
    public void TestRotate()
    {
        head.position = look.forward * 2f;
    }

    public void Rotate()
    {
        var angle = QuaternionTo(look.position, tf_Start.position);
        look.rotation = Quaternion.RotateTowards(look.rotation, angle, 180);
        // look.eulerAngles += new Vector3(0f, 0f, -90f);

        // Vector3 vectorToTarget = tf_Start.position - look.position;
        // float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        // Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        // look.rotation = Quaternion.Slerp(transform.rotation, q, 1);
    }

    public float AngleTo(Vector2 origin, Vector2 target)
    {
        Vector2 diference = target - origin;
        float signY = (target.y < origin.y) ? -1.0f : 1.0f;

        return Vector2.Angle(origin, diference) * signY;
    }

    public Quaternion QuaternionTo(Vector3 origin, Vector3 target)
    {
        Vector3 myLocation = origin;
        Vector3 targetLocation = target;
        targetLocation.z = myLocation.z; // ensure there is no 3D rotation by aligning Z position

        // vector from this object towards the target location
        Vector3 vectorToTarget = targetLocation - myLocation;
        // rotate that vector by 90 degrees around the Z axis
        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 0) * new Vector3(vectorToTarget.x, vectorToTarget.y, vectorToTarget.z + 180f);

        // get the rotation that points the Z axis forward, and the Y axis 90 degrees away from the target
        // (resulting in the X axis facing the target)
        Quaternion targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: rotatedVectorToTarget);

        return targetRotation;
    }

    [Sirenix.OdinInspector.Button]
    public void RegexString()
    {
        string text = "Foobar_test";
        Regex regex = new Regex(@"^.*?(?=_)");
        Match result = Regex.Match(text, @"^(?=_).*?");
        Debug.Log(text.Split('_').Skip(1).First());
        Debug.Log(text.Split('_').First());
    }
}
