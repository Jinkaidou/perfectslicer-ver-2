using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;

public class AnalysticsManager : Singleton<AnalysticsManager>
{
    public static void LogPlayLevel(int _level)
    {
        string eventName = "play_level";
        string parameterName = "level";
        string value = _level.ToString();

        FirebaseManager.Instance.LogAnalyticsEvent(eventName, parameterName, value);
    }

    public static void LogOpenApp()
    {
        string eventName = "open_app";

        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }

    public static void LogAdsInterDisplayed()
    {
        string eventName = "af_inters";

        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }

    public static void LogAdsInterCall()
    {
        string eventName = "inters_attempt";

        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }

    public static void LogAdsRewardVideoDisplayed()
    {
        string eventName = "af_rewarded";

        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }

    public static void LogAdsRewardVideoCall()
    {
        string eventName = "rewarded_attempt";

        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }
}