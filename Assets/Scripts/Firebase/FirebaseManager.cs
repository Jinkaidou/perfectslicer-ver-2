using System.Threading.Tasks;
using System;
using Firebase.Analytics;
using Firebase.RemoteConfig;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Firebase.Extensions;
using Firebase.Crashlytics;
// using System.Threading.Tasks;

public class FirebaseManager : Singleton<FirebaseManager>
{
    private const float DEFAULT_LOADING_TIME = 2;
    // private static FirebaseManager m_Instance = null;
    public bool IsLoaded = false;
    //private FirebaseDB m_firebaseDb;
    // public static FirebaseManager Instance
    // {
    //     get
    //     {
    //         return m_Instance;
    //     }
    // }
    private FirebaseAnalyticsManager m_FirebaseAnalyticsManager;
    // private FirebaseRemoteConfigManager m_FirebaseRemoteConfigManager;
    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;

    [Header("Remote Config")]
    public FirebaseRemoteConfig remoteConfig;

    private void Start()
    {
        // m_Instance = this;
        // DontDestroyOnLoad(gameObject);
        IsLoaded = false;
        Init();
    }
    public void Init()
    {
        if (m_FirebaseAnalyticsManager == null)
        {
            m_FirebaseAnalyticsManager = new FirebaseAnalyticsManager();
        }
        // if (m_FirebaseRemoteConfigManager == null)
        // {
        //     m_FirebaseRemoteConfigManager = new FirebaseRemoteConfigManager();
        // }
        Debug.Log("Start Config");
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // remoteConfig = FirebaseRemoteConfig.DefaultInstance;
                InitializeFirebase();
                FirebaseRemoteConfigManager.Instance.InitializeFirebase();
                FirebaseCrashlytics.Instance.InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void InitializeFirebase()
    {
        IsLoaded = true;
        // Debug.Log("Successsssssssssss");
        // System.Collections.Generic.Dictionary<string, object> defaults =
        //     new System.Collections.Generic.Dictionary<string, object>();

        // defaults.Add("open_aoa", true);
        // defaults.Add("inter_min_time", 15);

        // remoteConfig.SetDefaultsAsync(defaults);

        //         Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults)
        //   .ContinueWithOnMainThread(task => { })

        // defaults.Add("aoa_enable", true);

        // remoteConfig.ActivateAsync();

        // Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults)
        // .ContinueWithOnMainThread(task =>
        // {
        //     // [END set_defaults]
        //     Debug.Log("REMOTEEEEEEEEEEEEEEEEEEEEEEEE");
        //     //   isFirebaseInitialized = true;

        // });

        // remoteConfig.SetDefaultsAsync(defaults)
        // .ContinueWithOnMainThread(task =>
        // {
        //     Debug.Log("REMOTEEEEEEEEEEEEEEEEEEEEEEEE");
        // });

        // remoteConfig.FetchAsync(TimeSpan.Zero).ContinueWith(async task =>
        // // remoteConfig.FetchAndActivateAsync().ContinueWith(task =>
        //         {
        //             // handle completion
        //             Debug.Log("REMOTEEEEEEEEEEEEEEEEEEEEEEEE");
        //             // ConfigValue open_aoa = remoteConfig.GetValue("open_aoa");
        //             // ConfigValue inter_min_time = remoteConfig.GetValue("inter_min_time");

        //             // await UniTask.WaitUntil(() => AppOpenAdManager.Instance != null);

        //             // AppOpenAdManager.Instance.canShowAd = open_aoa.BooleanValue;

        //             // ProfileManager.MyProfile.m_InterTime.Reset(); 
        //             // ProfileManager.MyProfile.m_InterTime = new TimeRefillUnit((int)config.DoubleValue, 1, 0, "inter_cd_time", "inter_cd_time");
        //             // ProfileManager.MyProfile.m_InterTime.Reset();

        //             // GameManager.Instance.m_InterTime = (int)config.DoubleValue;

        //             // Helper.DebugLog("open_aoa: " + remoteConfig.GetValue("open_aoa").BooleanValue.ToString());
        //             // Helper.DebugLog("Config Value: " + config.DoubleValue.ToString());
        //             // ProfileManager.MyProfile.m_InterTime = (TimeRefillUnit)config.DoubleValue;
        //         });


        // remoteConfig.FetchAsync();
        // m_FirebaseRemoteConfigManager.SetupDefaultConfigs();
        // FetchData(() =>
        // {
        //     EventManager.TriggerEvent("UpdateRemoteConfigs");
        // });
    }

    public bool IsFirebaseReady()
    {
        return IsLoaded;
    }

    public void LogAnalyticsEvent(string eventName, string eventParameter, double eventValue)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.LogEvent(eventName, eventParameter, eventValue);
        }
    }
    public void LogAnalyticsEvent(string eventName, string eventParameter, string eventValue)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.LogEvent(eventName, eventParameter, eventValue);
        }
    }
    public void LogAnalyticsEvent(string eventName, Parameter[] paramss)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.LogEvent(eventName, paramss);
        }
    }
    public void LogAnalyticsEvent(string eventName)
    {
        if (IsFirebaseReady())
        {
            Debug.Log("Firebase init");
            m_FirebaseAnalyticsManager.LogEvent(eventName);
        }
    }
    public void SetUserProperty(string propertyName, string property)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.SetUserProperty(propertyName, property);
        }
    }
}