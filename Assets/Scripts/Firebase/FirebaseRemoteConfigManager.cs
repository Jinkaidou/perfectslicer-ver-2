using Firebase.Extensions;
using UnityEngine;

[DefaultExecutionOrder(-99)]
public class FirebaseRemoteConfigManager : Singleton<FirebaseRemoteConfigManager>
{
    public bool isFirebaseInitialized = false;

    public void InitializeFirebase()
    {
        isFirebaseInitialized = true;
    }
}
