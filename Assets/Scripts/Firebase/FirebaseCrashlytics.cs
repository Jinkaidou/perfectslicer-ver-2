// Copyright 2018 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


using Firebase;
using Firebase.Crashlytics;
using Firebase.Extensions;
using System;
using UnityEngine;

// Handler for UI buttons on the scene.  Also performs some
// necessary setup (initializing the firebase app, etc) on
// startup.
[DefaultExecutionOrder(-99)]
public class FirebaseCrashlytics : Singleton<FirebaseCrashlytics>
{
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    public bool firebaseInitialized = false;

    // When the app starts, check to make sure that we have
    // the required dependencies to use Firebase, and if not,
    // add them if possible.
    //public virtual void Start()
    //{
    //    FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
    //    {
    //        dependencyStatus = task.Result;
    //        if (dependencyStatus == DependencyStatus.Available)
    //        {
    //            InitializeFirebase();
    //        }
    //        else
    //        {
    //            Debug.LogError(
    //              "Could not resolve all Firebase dependencies: " + dependencyStatus);
    //        }
    //    });
    //}

    // Handle initialization of the necessary firebase modules:
    public void InitializeFirebase()
    {
        firebaseInitialized = true;
    }

    // Causes an error that will crash the app at the platform level (Android or iOS)
    public void ThrowUncaughtException()
    {
        DebugLog("Causing a platform crash.");
        throw new InvalidOperationException("Uncaught exception created from UI.");
    }

    // Log a caught exception.
    public void LogCaughtException()
    {
        DebugLog("Catching an logging an exception.");
        try
        {
            throw new InvalidOperationException("This exception should be caught");
        }
        catch (Exception ex)
        {
            Crashlytics.LogException(ex);
        }
    }

    // Write to the Crashlytics session log
    public void WriteCustomLog(String s)
    {
        DebugLog("Logging message to Crashlytics session: " + s);
        Crashlytics.Log(s);
    }

    // Add custom key / value pair to Crashlytics session
    public void SetCustomKey(String key, String value)
    {
        DebugLog("Setting Crashlytics Custom Key: <" + key + " / " + value + ">");
        Crashlytics.SetCustomKey(key, value);
    }

    // Set User Identifier for this Crashlytics session 
    public void SetUserID(String id)
    {
        DebugLog("Setting Crashlytics user identifier: " + id);
        Crashlytics.SetUserId(id);
    }

    // Output text to the debug log text field, as well as the console.
    public void DebugLog(string s)
    {
        print(s);
    }
}
