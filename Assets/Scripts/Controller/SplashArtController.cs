using Base.Core;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UI.LoadingScene;
using UnityEngine;

namespace UI.GamePlay
{

    public class SplashArtController : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            DOVirtual.DelayedCall(5f, () =>
            {
                GameManager.Instance.LoadSenceNew(SceneName.HomeScene);
            });
        }
    }
}