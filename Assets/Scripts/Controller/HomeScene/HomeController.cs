using Base.Core;
using System.Collections;
using TMPro;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;
using DataAccount;
using GamePlay.UI;
using Base.Core.Sound;

namespace UI.GamePlay
{

    public class HomeController : MonoBehaviour
    {
        [SerializeField] private Button settingBtn;
        [SerializeField] private Button dailyCheckinBtn;
        [SerializeField] private Button collectionBtn;
        [SerializeField] private Button ShopBtn;
        [SerializeField] private Button newGameBtn;
        [SerializeField] private Button playBtn;
        [SerializeField] private TextMeshProUGUI moneyTxt;
        [SerializeField] private TextMeshProUGUI levelTxt;
        [SerializeField] private DailyCheckinPopupController DailyCheckinPopup;
        [SerializeField] private SettingController setting;

        private void Awake()
        {
            ListenerButton();
            InitDataBase();
        }

        private void ListenerButton()
        {
            settingBtn.onClick.AddListener(OnClickSetting);
            dailyCheckinBtn.onClick.AddListener(OnClickDailyCheckin);
            collectionBtn.onClick.AddListener(OnClickCollection);
            ShopBtn.onClick.AddListener(OnClickShop);
            newGameBtn.onClick.AddListener(OnClickNewGame);
            playBtn.onClick.AddListener(OnClickPlay);
        }

        private void InitDataBase()
        {
            var money = DataAccountPlayer.PlayerMoneyData.currentmoney;
            var level = DataAccountPlayer.PlayerProcessData.currentLevel;
            moneyTxt.text = money.ToString();
            levelTxt.text = "Level " + level.ToString();

        }

        private void OnClickPlay()
        {
            PlaySound();
            GameManager.Instance.LoadScene(SceneName.GamePlay);
        }

        private void OnClickSetting()
        {
            PlaySound();
            setting.gameObject.SetActive(true);
        }

        private void OnClickDailyCheckin()
        {
            PlaySound();
            DailyCheckinPopup.gameObject.SetActive(true);
        }

        private void OnClickCollection()
        {
            PlaySound();
            GameManager.Instance.LoadSenceNew(SceneName.CollectionScene);
        }

        private void OnClickShop()
        {
            PlaySound();
            GameManager.Instance.LoadSenceNew(SceneName.ShopScene);
        }

        private void OnClickNewGame()
        {

        }

        private void PlaySound()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
        }
    }
}