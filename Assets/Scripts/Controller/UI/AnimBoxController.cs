﻿using DataAccount;
using Lofelt.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UI.GamePlay
{
    public class AnimBoxController : MonoBehaviour
    {
       [SerializeField] private Animator BoxOperater;
       [SerializeField] private Animator loopFx;
        private bool Ishaptic;
        private int timeCount;
        public void OpenBox()
        {
            BoxOperater.Play("OpenBox");
            Ishaptic = true;
            loopFx.gameObject.SetActive(true);
            StartCoroutine(TimeCounting());
        }

        private void OnDisable()
        {
            this.gameObject.SetActive(true);
        }

        private void OnEnable()
        {
            loopFx.gameObject.SetActive(false);
        }

        public void EndingAnim()
        {
            Ishaptic = false;
            loopFx.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
            StopAllCoroutines();
        }

        public IEnumerator TimeCounting()
        {
            while (Ishaptic)
            {
                timeCount += 1;
                var isHaptic = DataAccountPlayer.PlayerSettings.VibrationOff;
                if (!isHaptic)
                {

                    HapticPatterns.PlayPreset(HapticPatterns.PresetType.HeavyImpact);
                    Debug.LogError("runggg liên tọi");
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}