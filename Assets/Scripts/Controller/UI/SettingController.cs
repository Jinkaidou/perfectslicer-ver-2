using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DataAccount;

namespace UI.GamePlay
{
    public class SettingController : MonoBehaviour
    {
        [SerializeField] private Button hapticChangeBtn;
        [SerializeField] private Button closeBtn;
        [SerializeField] private Image tickSelect;
        [SerializeField] private Slider volumLevel;

        private int countPress;

        private void Awake()
        {
            volumLevel.maxValue = 1;
            volumLevel.value = DataAccountPlayer.PlayerSettings.volume;
            ListenerBtn();
        }

        private void ListenerBtn()
        {
            hapticChangeBtn.onClick.AddListener(OnClickHaptic);
            closeBtn.onClick.AddListener(OnClickClose);
        }

        private void OnClickHaptic()
        {
            countPress += 1;
            if (countPress % 2 == 0)
            {
                tickSelect.gameObject.SetActive(true);
                DataAccountPlayer.PlayerSettings.SetVibration(false);
            }
            else
            {
                tickSelect.gameObject.SetActive(false);
                DataAccountPlayer.PlayerSettings.SetVibration(true);
            }
        }

        private void OnClickClose()
        {
            gameObject.SetActive(false);
            Debug.LogError("volume = " + volumLevel.value);
            DataAccountPlayer.PlayerSettings.SetVolumValue(volumLevel.value);
        }
    }
}