﻿using DataAccount;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GamePlay.UI
{
    public class UnlockElementController : MonoBehaviour
    {
        [SerializeField] private Slider processUnlock;
        [SerializeField] private Image itemUnlock;
        [SerializeField] private TextMeshProUGUI processPercent;
        private int _idItem;
        public void InitData(int cost, string pathImg, int id, int currentLevel, bool isCup)
        {
            processUnlock.maxValue = cost;
            processUnlock.value = currentLevel;
            itemUnlock.sprite = Resources.Load<Sprite>(pathImg);
            _idItem = id;
            var process = ((float)currentLevel / (float)cost); ;
            var processPercentLoad = process * 100;

            processPercent.text = Mathf.RoundToInt(processPercentLoad).ToString() + "%";
            if (cost == currentLevel)
            {
                SetStatusItem(isCup);
                processPercent.text = 100.ToString() + "%";
            }

            //itemUnlock.SetNativeSize();
            Debug.Log(id + " item : price " + cost);
        }

        private void SetStatusItem(bool isCup)
        {
            if (isCup)
            {
                var cup = new PlayerCollectionCup(_idItem, false);
                var cupList = new PlayerCollectionCupList();
                cupList.Add(cup);
                DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);
            }
            else
            {
                var knife = new PlayerCollectionKnife(_idItem, false);
                var knifeList = new PlayerCollectionKnifeList();
                knifeList.Add(knife);
                DataAccountPlayer.PlayerCollectionData.ClaimKnife(knifeList);
            }
        }
    }
}