﻿using Base.Core;
using TMPro;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;
using DataAccount;
using JinLab.DataBase;
using Controller.LoadData;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Base.Core.Sound;
using DG.Tweening;

namespace GamePlay.UI
{
    public class UnlockPopupController : MonoBehaviour
    {
        [SerializeField] private Button claimMoneyStage;
        [SerializeField] private Button claimMoneyStageAds;
        [SerializeField] private TextMeshProUGUI moneyStage;
        [SerializeField] private TextMeshProUGUI moneyStageAds;

        [SerializeField] private UnlockElementController unlockElementController;


        private List<float> costCup = new List<float>();
        private List<float> costKnife = new List<float>();

        private ListCollectionData _listCollectionData;
        private int _currentLv;

        private float _closetPriceCup;
        private float _closetPriceKnife;

        private void Awake()
        {
            _currentLv = DataAccountPlayer.PlayerProcessData.currentLevel;
            ShowMoneyHave();
            ButtonListener();
            InitDataBase();
            //this.RegisterListener(EventID.OnShowReward, (sender, param) => ShowAdsReward());
        }

        private void OnEnable()
        {
            claimMoneyStage.gameObject.SetActive(false);
            DOVirtual.DelayedCall(1.5f, () =>
              claimMoneyStage.gameObject.SetActive(true)
         );
        }

        private void ButtonListener()
        {
            claimMoneyStage.onClick.AddListener(OnclickClaimStage);
            claimMoneyStageAds.onClick.AddListener(OnclickClaimStageAds);
        }

        private void ShowMoneyHave()
        {
           var stageMoney =   DataAccountPlayer.PlayerMoneyData.currentMoneyOnStage;
           var stageMoneyAds =   DataAccountPlayer.PlayerMoneyData.currentMoneyOnAds;

            moneyStage.text = "+" +  stageMoney.ToString();
            moneyStageAds.text = "+" + stageMoneyAds.ToString();
        }

        private void OnclickClaimStage()
        {
            AdsManager.Instance.ShowAdsInter();
            this.gameObject.SetActive(false);

            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.coin_pickup);
            }

            GameManager.Instance.LoadSenceNew(SceneName.HomeScene);
            Time.timeScale = 1;
            DataAccountPlayer.PlayerMoneyData.IncreaseMoneyStage();


        }

        private void OnclickClaimStageAds()
        {
            AdsManager.Instance.ShowAdsReward(() => ShowAdsReward(), "");
        }

        private void ShowAdsReward()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.coin_pickup);
            }
            this.gameObject.SetActive(false);
            GameManager.Instance.LoadScene(SceneName.GamePlay);
            Time.timeScale = 1;
            DataAccountPlayer.PlayerMoneyData.IncreaseMoneyAds();
        }

        private void InitDataBase()
        {
            _listCollectionData = LoadResourceController.Instance.LoadDataCollection();
            var knifeCollect = _listCollectionData.collectiveKnife;
            var cupCollect = _listCollectionData.collectiveCup;

            for (int i = 0; i < knifeCollect.Count; i++)
            {
                if (knifeCollect[i].typeUnlock == TypeCose.UnlockFromCampaign)
                {
                    var cost = (int)knifeCollect[i].cost;
                    if(cost >= _currentLv)
                    {
                        costKnife.Add(cost);
                    }
                }
            }

            for (int i = 0; i < cupCollect.Count; i++)
            {
                if (cupCollect[i].typeUnlock == TypeCose.UnlockFromCampaign)
                {
                    var cost = (int)cupCollect[i].cost;
                    if(cost >= _currentLv)
                    {
                        costCup.Add(cost);
                    }
                }
            }

             _closetPriceKnife = costKnife.Aggregate((x, y) => Math.Abs(x - _currentLv) < Math.Abs(y - _currentLv) ? x : y);
             _closetPriceCup = costCup.Aggregate((x, y) => Math.Abs(x - _currentLv) < Math.Abs(y - _currentLv) ? x : y);

            if (_closetPriceCup < _closetPriceKnife)
            {
                InitDataImageCup(cupCollect);
            }
            else if (_closetPriceCup > _closetPriceKnife)
            {
                InitDataImageKnife(knifeCollect);
            }
        }

        private void InitDataImageCup(List<CollectionData> cupCollect)
        {
            for (int i = 0; i < cupCollect.Count; i++)
            {
                if (cupCollect[i].typeUnlock == TypeCose.UnlockFromCampaign)
                {
                    var cost = (int)cupCollect[i].cost;
                    if (cost == _closetPriceCup)
                    {
                        var id = cupCollect[i].Id;
                        var path = $"Sprite/CupItems/{id.ToString()}";
                        unlockElementController.InitData(cost, path, id, _currentLv,true);
                    }
                }
            }
        }

        private void InitDataImageKnife(List<CollectionData> knifeCollect)
        {
            for (int i = 0; i < knifeCollect.Count; i++)
            {
                if (knifeCollect[i].typeUnlock == TypeCose.UnlockFromCampaign)
                {
                    var cost = (int)knifeCollect[i].cost;
                    if (cost == _closetPriceKnife)
                    {
                        var id = knifeCollect[i].Id;
                        var path = $"Sprite/KnifeItems/{id.ToString()}";
                        unlockElementController.InitData(cost, path, id, _currentLv,false);
                    }

                }
            }
        }

    }
}