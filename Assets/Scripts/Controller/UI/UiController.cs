﻿using Base.Core;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;
using DataAccount;
using TMPro;
using AppsFlyerSDK;
using Base.Core.Sound;

namespace GamePlay.UI
{
    public class UiController : MonoBehaviour
    {
        [SerializeField] private GameObject VictoryPopup;
        [SerializeField] private GameObject FailedPopup;

        [SerializeField] private ContinuePopupController revivePopup;
        [SerializeField] private GameObject unlockPopup;

        [SerializeField] private int DeactivePopupResult;
        [SerializeField] private Button reloadGameBtn;
        [SerializeField] private TextMeshProUGUI coin ;

        [SerializeField] private Image blockPanel;
        [SerializeField] private GameObject tutorial;
        [SerializeField] private Button closeTut;
        private int _countFalse;

        //cheat UI
        [SerializeField] private GameObject gold;
        [SerializeField] private GameObject reset;

        [SerializeField] private Button cheatUI;
        private int countCheat;

        private void Awake()
        {
            DeactivePopup();
            InitLogicEnd();
            InitTutorial();

            reloadGameBtn.onClick.AddListener(RealoadGame);
            closeTut.onClick.AddListener(CloseTut);
            cheatUI.onClick.AddListener(CheatUI);

            coin.text = DataAccountPlayer.PlayerMoneyData.currentmoney.ToString();



            this.RegisterListener(EventID.WinGame, (sender, param) =>
            {
                ActivatePanel();
            });
        }

        private void RealoadGame()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
            AdsManager.Instance.ShowAdsInter();
            GameManager.Instance.LoadSenceNew(SceneName.HomeScene);
            AppsFlyer.sendEvent($"reroll_interact_fruit_{GameController.Instance.idFruit}", null);
            FirebaseManager.Instance.LogAnalyticsEvent($"reroll_interact_fruit_{GameController.Instance.idFruit}");
            AppsFlyer.sendEvent($"replay_level_{DataAccountPlayer.PlayerProcessData.currentLevel}", null);
            FirebaseManager.Instance.LogAnalyticsEvent($"replay_level_{DataAccountPlayer.PlayerProcessData.currentLevel}");
        }

        private void ActivatePanel()
        {
            blockPanel.gameObject.SetActive(true);
            {
                LeanTween.delayedCall(1, () =>
                {
                    blockPanel.gameObject.SetActive(false);
                });
            }
        }

        private void CheatUI()
        {
            countCheat += 1;
            if(countCheat % 2 != 0)
            {
                gold.SetActive(false);
                reset.SetActive(false);

                gold.SetActive(false);
            }
            else if(countCheat % 2 == 0)
            {
                gold.SetActive(gold);
                reset.SetActive(gold);

                gold.SetActive(gold);
            }
        }

        private void CloseTut()
        {
            tutorial.gameObject.SetActive(false);
        }

        private void InitTutorial()
        {
           var level =  DataAccountPlayer.PlayerProcessData.currentLevel;
            if(level <= 1)
            {
                tutorial.gameObject.SetActive(true);
            }
            else
            {
                tutorial.gameObject.SetActive(false);
            }
        }

        private void DeactivePopup()
        {
            VictoryPopup.SetActive(false);
            FailedPopup.SetActive(false);
            revivePopup.gameObject.SetActive(false);
            unlockPopup.SetActive(false);
        }

        private void InitLogicEnd()
        {
            this.RegisterListener(EventID.LoseGame, (sender, param) =>
            {
                ActiveLoseThread();
            });

            this.RegisterListener(EventID.WinGame, (sender, param) =>
            {
                ActiveWinThread();
            });
        }

        private void ActiveWinThread()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.victory);
            }

            VictoryPopup.SetActive(true);
            LeanTween.delayedCall(0.5f, () =>
            {
                //VictoryPopup.SetActive(true);
            });
            LeanTween.delayedCall(DeactivePopupResult, () =>
            {
                VictoryPopup.SetActive(false);
                unlockPopup.SetActive(true);
                PauseGame();
            });
            DataAccountPlayer.PlayerProcessData.IncreaseLevel();
            DataAccountPlayer.PlayerMoneyData.CalculateMoneyProcess();
            AppsFlyer.sendEvent($"win_level_{DataAccountPlayer.PlayerProcessData.currentLevel}", null);
            FirebaseManager.Instance.LogAnalyticsEvent($"win_level_{DataAccountPlayer.PlayerProcessData.currentLevel}");
        }

        private void ActiveLoseThread()
        {
            FailedPopup.SetActive(true);
            if (_countFalse <= 0)
            {
                LeanTween.delayedCall(DeactivePopupResult, () =>
                {
                    FailedPopup.SetActive(false);
                    _countFalse += 1;
                    AppsFlyer.sendEvent($"lose_level_{DataAccountPlayer.PlayerProcessData.currentLevel}", null);
                    FirebaseManager.Instance.LogAnalyticsEvent($"lose_level_{DataAccountPlayer.PlayerProcessData.currentLevel}");
                    revivePopup.gameObject.SetActive(true);
                    //PauseGame();
                });

            }
            else
            {
                LeanTween.delayedCall(DeactivePopupResult, () =>
                {
                    AdsManager.Instance.ShowAdsInter();
                    Time.timeScale = 1;
                    Debug.Log("load lại scene lose");
                    GameController.Instance.tranferInformation = true;
                    this.gameObject.SetActive(false);
                    _countFalse = 0;
                    GameManager.Instance.LoadSenceNew(SceneName.HomeScene);
                    GameController.Instance.tranferInformation = true;
                });
            }
        }

        public void PauseGame()
        {
            Time.timeScale = 0;
        }
    }
}