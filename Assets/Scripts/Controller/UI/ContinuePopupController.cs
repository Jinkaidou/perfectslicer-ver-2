﻿using AppsFlyerSDK;
using Base.Core;
using DataAccount;
using System.Collections;
using TMPro;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;

namespace GamePlay.UI
{
    public class ContinuePopupController : MonoBehaviour
    {
        [SerializeField] private Button okBtn;
        [SerializeField] private Button closeBtn;
        [SerializeField] private Image fillHeart;

        private int timeCount;

        private void Awake()
        {
            ListenerBtn();
        }

        private void ListenerBtn()
        {
            okBtn.onClick.AddListener(OnclickOk);
            closeBtn.onClick.AddListener(OnclickClose);
        }

        private void OnclickOk()
        {
            AppsFlyer.sendEvent("level_continue_reward_ad", null);
            FirebaseManager.Instance.LogAnalyticsEvent($"level_continue_reward_ad");

            AppsFlyer.sendEvent($"level{DataAccountPlayer.PlayerProcessData.currentLevel}_continue_reward_ad", null);
            FirebaseManager.Instance.LogAnalyticsEvent($"level{DataAccountPlayer.PlayerProcessData.currentLevel}_continue_reward_ad");

            AdsManager.Instance.ShowAdsReward(() => ShowAdsReward(), "");
        }

        private void ShowAdsReward()
        {
            Time.timeScale = 1;
            GameController.Instance.tranferInformation = true;
            this.gameObject.SetActive(false);
        }

        public void OnclickClose()
        {
            //load lại scene
            AdsManager.Instance.ShowAdsInter();
            Time.timeScale = 1;
            Debug.Log("load lại scene lose");
            GameController.Instance.tranferInformation = true;
            this.gameObject.SetActive(false);
            GameManager.Instance.LoadSenceNew(SceneName.HomeScene);
            GameController.Instance.tranferInformation = true;
        }

    }
}