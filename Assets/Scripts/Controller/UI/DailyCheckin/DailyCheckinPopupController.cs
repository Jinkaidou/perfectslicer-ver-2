using Base.Core;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;
using DataAccount;
using TMPro;
using System.Collections.Generic;
using JinLab.DataBase;
using Controller.LoadData;
using System.Linq;
using Base.Core.Sound;

namespace GamePlay.UI
{
    public class DailyCheckinPopupController : MonoBehaviour
    {
        [SerializeField] private Button okBtn;
        [SerializeField] private Button cheatDay;
        [SerializeField] private VerticalLayoutGroup content;
        [SerializeField] private DailyCheckinItemController dailyCheckinItemController;
        public List<DailyCheckinItemController> listDailyCheckinItemControllers =  new List<DailyCheckinItemController>();
        public List<float> listDaily =  new List<float>();
        //DataBase

        private ListCollectionData _listCollectionData;

        private void Awake()
        {
            LisenterButton();
            InitData();
        }

        private void LisenterButton()
        {
            okBtn.onClick.AddListener(OnClickOkBtn);
            cheatDay.onClick.AddListener(CheatOneDay);
        }

        private void InitData()
        {
            _listCollectionData = LoadResourceController.Instance.LoadDataCollection();
            var knifeCollect = _listCollectionData.collectiveKnife;
            var cupCollect = _listCollectionData.collectiveCup;

            for(int i = 0; i < knifeCollect.Count; i++)
            {
                if(knifeCollect[i].typeUnlock == TypeCose.UnlockDailyCheckin)
                {
                    var cost = (int)knifeCollect[i].cost;
                    var id = knifeCollect[i].Id;
                    var path = $"Sprite/KnifeItems/{id.ToString()}";

                    CreateItemCheckinElement(cost, path, id, false);
                }
            }

            for (int i = 0; i < cupCollect.Count; i++)
            {
                if (cupCollect[i].typeUnlock == TypeCose.UnlockDailyCheckin)
                {
                    var cost = (int)cupCollect[i].cost;
                    var id = cupCollect[i].Id;
                    var path = $"Sprite/CupItems/{id.ToString()}";

                    CreateItemCheckinElement(cost,path,id, true);
                }
            }

            for(int i = 0; i < listDailyCheckinItemControllers.Count; i++)
            {
                listDailyCheckinItemControllers[i].transform.SetParent(content.transform);
                listDailyCheckinItemControllers[i].transform.localScale = new Vector3(0.26f, 0.2f, 1);
            }
        }

        private void CreateItemCheckinElement(int cost, string pathImg, int id, bool check)
        {
            var item = Instantiate(dailyCheckinItemController);
            item.InitData(cost,pathImg,id,check);
            listDailyCheckinItemControllers.Add(item);

            listDailyCheckinItemControllers.OrderByDescending(o => o.dayLimit).ToList();
            listDailyCheckinItemControllers.Sort((x, y) => x.dayLimit.CompareTo(y.dayLimit));
            // listDailyCheckinItemControllers.OrderByDescending(daily => dailyCheckinItemController.dayLimit);
        }

        private void OnClickOkBtn()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
            this.gameObject.SetActive(false);
        }

        private void CheatOneDay()
        {
            DataAccountPlayer.PlayerProcessData.SetDayLimit();
        }

    }
}