using Base.Core;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;
using DataAccount;
using TMPro;
using AppsFlyerSDK;
using Base.Core.Sound;

namespace GamePlay.UI
{
    public class DailyCheckinItemController : MonoBehaviour
    {
        [SerializeField] private Image previewReward;
        [SerializeField] private Image progressBar;
        [SerializeField] private TextMeshProUGUI progressTxt;
        [SerializeField] private Button claimAdsRewardBtn;
        [SerializeField] private GameObject tickGreen;

        public int dayLimit;
        private int _id;

        private bool _isCup;
        private string pathLoad;

        private void Awake()
        {
            LisenterButton();

        }

        private void OnEnable()
        {
            //claimAdsRewardBtn.gameObject.SetActive(false);
            RegisterTimeCheckin();
            //tickGreen.gameObject.SetActive(false);
            ShowIsAlreadyHave();
        }

        private void RegisterTimeCheckin()
        {
            var checkin = DataAccountPlayer.PlayerProcessData.IsDayClaimable();
            var day = DataAccountPlayer.PlayerProcessData.day;
            if (checkin || day <= 0)
            {
                DataAccountPlayer.PlayerProcessData.SetTimeCheckin();
                DataAccountPlayer.PlayerProcessData.SetDayLimit();
                PostEventAppFlyers();
            }
        }

        private void PostEventAppFlyers()
        {
            if(DataAccountPlayer.PlayerProcessData.day == 3)
            {
                AppsFlyer.sendEvent("daily_checkin_interact_3_days", null);
                FirebaseManager.Instance.LogAnalyticsEvent($"daily_checkin_interact_3_days");
            }
            if (DataAccountPlayer.PlayerProcessData.day == 5)
            {
                AppsFlyer.sendEvent("daily_checkin_interact_5_days", null);
                FirebaseManager.Instance.LogAnalyticsEvent($"daily_checkin_interact_5_days");
            }
            if (DataAccountPlayer.PlayerProcessData.day == 7)
            {
                AppsFlyer.sendEvent("daily_checkin_interact_7_days", null);
                FirebaseManager.Instance.LogAnalyticsEvent($"daily_checkin_interact_7_days");
            }
            if (DataAccountPlayer.PlayerProcessData.day == 14)
            {
                AppsFlyer.sendEvent("daily_checkin_interact_14_days", null);
                FirebaseManager.Instance.LogAnalyticsEvent($"daily_checkin_interact_14_days");
            }
            if (DataAccountPlayer.PlayerProcessData.day == 30)
            {
                AppsFlyer.sendEvent("daily_checkin_interact_30_days", null);
                FirebaseManager.Instance.LogAnalyticsEvent($"daily_checkin_interact_30_days");
            }
        }

        public void InitData(int dayLimit, string pathImg, int id, bool isCup)
        {
            _isCup = isCup;
            pathLoad = pathImg;
            previewReward.sprite = Resources.Load<Sprite>(pathLoad);
            previewReward.SetNativeSize();
            this.dayLimit = dayLimit;
            _id = id;
            ShowProgress();
            ShowIsAlreadyHave();
        }

        private void ShowProgress()
        {
            progressTxt.text = DataAccountPlayer.PlayerProcessData.day.ToString() +  "/" + dayLimit.ToString() + " day";
            var percent = (float)DataAccountPlayer.PlayerProcessData.day / (float)dayLimit;
            progressBar.fillAmount = percent;
        }

        private void ShowStatusCheckin()
        {
            if ((float)DataAccountPlayer.PlayerProcessData.day >= (float)dayLimit)
            {
                claimAdsRewardBtn.gameObject.SetActive(true);
            }
            else
            {
                claimAdsRewardBtn.gameObject.SetActive(false);
            }
        }

        private void ShowIsAlreadyHave()
        {
            if (_isCup)
            {
                var cupData = DataAccountPlayer.PlayerCollectionData.cupData.Find(x => x.id == _id);
                if (cupData == null)
                {
                    tickGreen.SetActive(false);
                    ShowStatusCheckin();
                }
                else
                {
                    tickGreen.SetActive(true);
                    claimAdsRewardBtn.gameObject.SetActive(false);
                }
            }
            else
            {
                var knifeData = DataAccountPlayer.PlayerCollectionData.knifeData.Find(x => x.id == _id);
                if (knifeData == null)
                {
                    tickGreen.SetActive(false);
                    ShowStatusCheckin();
                }
                else
                {
                    tickGreen.SetActive(true);
                    claimAdsRewardBtn.gameObject.SetActive(false);
                }
            }
        }

        private void LisenterButton()
        {
            claimAdsRewardBtn.onClick.AddListener(OnClickClaimReward);
        }

        private void OnClickClaimReward()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
            AdsManager.Instance.ShowAdsReward(() => ShowAdsReward(), "");
        }

        private void ShowAdsReward()
        {
            if (_isCup)
            {
                ClaimCup();
            }
            else
            {
                ClaimKnife();
            }
            ShowIsAlreadyHave();
        }

        private void ClaimCup()
        {
            var cup = new PlayerCollectionCup(_id, true);
            var cupList = new PlayerCollectionCupList();
            cupList.Add(cup);
            DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);
            DataAccountPlayer.PlayerDataAccount.SetCupID(_id, pathLoad);
        }

        private void ClaimKnife()
        {
            var knife = new PlayerCollectionKnife(_id, true);
            var knifeList = new PlayerCollectionKnifeList();
            knifeList.Add(knife);
            DataAccountPlayer.PlayerCollectionData.ClaimKnife(knifeList);
            DataAccountPlayer.PlayerDataAccount.SetKnifeID(_id, pathLoad);
        }
    }
}