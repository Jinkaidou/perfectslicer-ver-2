using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.GamePlay
{
    public class AnimCatController : MonoBehaviour
    {
        public Animator idle1;
        private bool changeAnim;


        public void ChangeStatusAnimCat()
        {
            if (changeAnim)
            {
                PlayAnimIdle1();
            }
            else
            {
                PlayAnimIdle2();
            }
        }


        public void PlayAnimIdle1()
        {
            changeAnim = false;
            idle1.Play("CatIdle1");
        }

        public void PlayAnimIdle2()
        {
            changeAnim = true;
            idle1.Play("CatIdle2");
        }
    }
}