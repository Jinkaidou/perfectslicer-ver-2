﻿using Base.Core;
using System.Collections;
using TMPro;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;
using DataAccount;
using JinLab.DataBase;
using Controller.LoadData;
using GamePlay.UI.Collection;
using System.Collections.Generic;
using System.Linq;
using AppsFlyerSDK;
using Base.Core.Sound;

namespace UI.GamePlay
{
    public class ShopController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI moneyTxt;
        private int _moneyShow;

        [SerializeField] private Button purchaseBtn;
        [SerializeField] private GameObject darkPanel;
        [SerializeField] private TextMeshProUGUI priceTxt;
        private int _pricePurchase;

        [SerializeField] private Button backBtn;

        [SerializeField] private Button moreMoneyBtn;
        [SerializeField] private Button cheatmoreMoneyBtn;
        [SerializeField] private TextMeshProUGUI bonusMoneyTxt;
        private int _moneyBonus;

        [SerializeField] private AnimCatController animCat;
        [SerializeField] private RewardClaimPopup rewardAds;

        private PriceCollectionData _priceData;
        private BonusMoneyData _bonusMoneyData;
        private int timeCount;
        private int _cupOrKnife;
        //DataBase

        private ListCollectionData listCollectionData;

        private void Awake()
        {
            ListenerButton();
            InitDataBase();
            StartCoroutine(TimeCounting());
        }

        private void ListenerButton()
        {
            purchaseBtn.onClick.AddListener(OnClickPurchaseBtn);
            backBtn.onClick.AddListener(OnClickBackBtn);
            moreMoneyBtn.onClick.AddListener(OnclickMoreMonyBtn);
            cheatmoreMoneyBtn.onClick.AddListener(OnclickCheatMoreMonyBtn);
        }

        private void PlaySound()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
        }

        private void InitDataBase()
        {

            InitTopBarMoney();


            _priceData = LoadResourceController.Instance.LoadDataPrice();
            InitPrice();

            _bonusMoneyData = LoadResourceController.Instance.LoadDataBonusMoney();
            InitDataBonus();

        }

        private void InitReward()
        {
            listCollectionData = LoadResourceController.Instance.LoadDataCollection();
            var isFirstTime = DataAccountPlayer.PlayerDataAccount.firstTimeGoShop;
            var cupCollect = listCollectionData.collectiveCup;
            var knifeCollect = listCollectionData.collectiveKnife;

            if (!isFirstTime)
            {
                DataAccountPlayer.PlayerDataAccount.ChangeShopStatus(true);

                for (int i = 0; i < knifeCollect.Count; i++)
                {
                    if (knifeCollect[i].typeUnlock == TypeCose.UnlockShop)
                    {
                        var id = knifeCollect[i].Id;
                        DataAccountPlayer.PlayerDataAccount.AddIdKnifeHas(id);
                    }
                }
                for (int i = 0; i < cupCollect.Count; i++)
                {
                    if (cupCollect[i].typeUnlock == TypeCose.UnlockShop)
                    {
                        var id = cupCollect[i].Id;
                        DataAccountPlayer.PlayerDataAccount.AddIdCupHas(id);
                    }
                }
            }



            _cupOrKnife = Random.Range(0, 100);
            if (_cupOrKnife % 2 == 0)
            {
                if (DataAccountPlayer.PlayerDataAccount.idKnifeHas.Count > 0)
                {
                    SetKnifeReward();
                }
                else
                {
                    SetCupReward();
                }
            }
            else if (_cupOrKnife % 2 != 0)
            {
                if (DataAccountPlayer.PlayerDataAccount.idCupHas.Count > 0)
                {
                    SetCupReward();
                }
                else
                {
                    SetKnifeReward();
                }
            }

        }

        private void SetKnifeReward()
        {
            _cupOrKnife = 2;
            var id = Random.Range(0, DataAccountPlayer.PlayerDataAccount.idKnifeHas.Count);
            AdsManager.Instance.rewardId = DataAccountPlayer.PlayerDataAccount.idKnifeHas[id];

            Debug.LogError(DataAccountPlayer.PlayerDataAccount.idKnifeHas.Count + " SO PHANTU DAO");
            Debug.LogError(DataAccountPlayer.PlayerDataAccount.idKnifeHas[id] + " id Dao");

            var knife = new PlayerCollectionKnife(DataAccountPlayer.PlayerDataAccount.idKnifeHas[id], true);
            var knifeList = new PlayerCollectionKnifeList();
            knifeList.Add(knife);

            DataAccountPlayer.PlayerCollectionData.ClaimKnife(knifeList);
            DataAccountPlayer.PlayerDataAccount.RemoveIdKnifeItem(id);
        }

        private void SetCupReward()
        {
            _cupOrKnife = 1;
            var id = Random.Range(0, DataAccountPlayer.PlayerDataAccount.idCupHas.Count);
            AdsManager.Instance.rewardId = DataAccountPlayer.PlayerDataAccount.idCupHas[id];

            Debug.LogError(DataAccountPlayer.PlayerDataAccount.idCupHas.Count + " SO PHANTU COC");
            Debug.LogError(DataAccountPlayer.PlayerDataAccount.idCupHas[id] + " id cup");


            var cup = new PlayerCollectionCup(DataAccountPlayer.PlayerDataAccount.idCupHas[id], true);
            var cupList = new PlayerCollectionCupList();
            cupList.Add(cup);

            DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);
            DataAccountPlayer.PlayerDataAccount.RemoveIdCupItem(id);
        }


        private void InitPrice()
        {
            var priceDatas = _priceData.priceDatas;
            for (int i = 0; i < priceDatas.Count; i++)
            {
                var levelPress = priceDatas[i].levelPress;
                var DataPlayerPurchase = DataAccountPlayer.PlayerProcessShopData.currentLevelPurchase;
                if(levelPress <= DataPlayerPurchase)
                {
                    _pricePurchase = priceDatas[i].price;
                    priceTxt.text = _pricePurchase.ToString();

                    if(DataAccountPlayer.PlayerMoneyData.currentmoney >= _pricePurchase)
                    {
                        darkPanel.gameObject.SetActive(false);
                    }
                    else
                    {
                        darkPanel.gameObject.SetActive(true);
                    }
                }
            }
            if (DataAccountPlayer.PlayerDataAccount.idKnifeHas.Count <= 0 && DataAccountPlayer.PlayerDataAccount.idCupHas.Count <= 0 && DataAccountPlayer.PlayerDataAccount.firstTimeGoShop)
            {
                darkPanel.SetActive(true);
            }
        }

        private void InitTopBarMoney()
        {
            var money = DataAccountPlayer.PlayerMoneyData.currentmoney;
            _moneyShow = money;
            moneyTxt.text = _moneyShow.ToString();
        }

        private void InitDataBonus()
        {
            var baseMoney = _bonusMoneyData.baseMoney;
            var bonusMoney = _bonusMoneyData.bonusMoney;
            var maxMoney = _bonusMoneyData.maxMoneyEarn;
            if (DataAccountPlayer.PlayerProcessShopData.currentLevelEarnMoney <= 0)
            {
                DataAccountPlayer.PlayerProcessShopData.SetMoneyEarnByAds(baseMoney, bonusMoney, maxMoney);
            }


            _moneyBonus = DataAccountPlayer.PlayerProcessShopData.moneyEarn;
            bonusMoneyTxt.text = _moneyBonus.ToString();
        }

        private void OnClickPurchaseBtn()
        {
            PlaySound();
            if (DataAccountPlayer.PlayerMoneyData.currentmoney >= _pricePurchase)
            {
                animCat.PlayAnimIdle1();
                DataAccountPlayer.PlayerMoneyData.ReduceMoney(_pricePurchase);
                InitTopBarMoney();
                DataAccountPlayer.PlayerProcessShopData.IncreaseLevelPurchase();

                AppsFlyer.sendEvent("shop_purchase_time", null);
                FirebaseManager.Instance.LogAnalyticsEvent($"shop_purchase_time");
                InitReward();
                if (_cupOrKnife % 2 != 0)
                {
                    rewardAds.OpenAdsClaimPopup(AdsManager.Instance.rewardId, CollectionTabType.CUP, false);
                }
                else if (_cupOrKnife % 2 == 0)
                {
                    rewardAds.OpenAdsClaimPopup(AdsManager.Instance.rewardId, CollectionTabType.KNIFE, false);
                }
                PurchaseItem();
                InitPrice();
            }
        }

        private void PurchaseItem()
        {
            rewardAds.gameObject.SetActive(true);
            rewardAds.CupOrKnife = _cupOrKnife;
        }

        private void OnClickBackBtn()
        {
            PlaySound();
            GameManager.Instance.LoadSenceNew(SceneName.HomeScene);
        }

        private void OnclickMoreMonyBtn()
        {
            PlaySound();
            AppsFlyer.sendEvent("shop_reward_ad", null);
            FirebaseManager.Instance.LogAnalyticsEvent($"shop_reward_ad");
            AdsManager.Instance.ShowAdsReward(() => ShowAdsReward(), "");
            //ShowAdsReward();
        }

        private void OnclickCheatMoreMonyBtn()
        {

            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            SoundManager.Instance.PlaySound(SoundType.coin_pickup);
            if (!isSound)
            {
                SoundManager.Instance.PlayOneShot(SoundType.coin_pickup);
            }

            DataAccountPlayer.PlayerMoneyData.IncreaseMoneyByValue(100000000);
            InitTopBarMoney();
            InitDataBonus();
            InitPrice();
        }

        private void ShowAdsReward()
        {
            Debug.LogError("tăng tiềnnnnnnnnnnnnnnnnnnn " + _moneyBonus);
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.coin_pickup);
            }
            DataAccountPlayer.PlayerMoneyData.IncreaseMoneyByValue(_moneyBonus);
            InitTopBarMoney();
            DataAccountPlayer.PlayerProcessShopData.IncreaseLevelEarnMoney();
            DataAccountPlayer.PlayerProcessShopData.IncreasMoneyEarn();
            InitDataBonus();
            InitPrice();
        }

        public IEnumerator TimeCounting()
        {
            while (true)
            {
                timeCount -= 1;
                if (timeCount <= 0)
                {
                    timeCount = 10;
                    animCat.ChangeStatusAnimCat();
                }
                yield return new WaitForSeconds(1f);
            }
        }
    }
}