﻿using Slicer2D;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JinLab.DataBase;
using Controller.LoadData;

namespace GamePlay
{
    public class FruitInteractController : SingletonMono<FruitInteractController>
    {
        public int idFruit;
        public GameObject fruitMap;
        private float numberDifference;
        public List<float> listValue = new List<float>();

        private MapCollectionData _mapData;
        private float _speed;

        private void Start()
        {
            InitDataBase();
            this.PostEvent(EventID.SaveIdFruit, idFruit);
            fruitMap = this.gameObject.transform.GetChild(0).gameObject;

        }

        private void Update()
        {
            Move();
        }

        private void InitDataBase()
        {
            _mapData = LoadResourceController.instance.LoadMapData();
        }

        private void Move()
        {
            if(GameController.instance.typeMove == TypeMove.UpDown)
            {
                MoveUpDown();
            }
            else if (GameController.instance.typeMove == TypeMove.LeftRight)
            {
                MoveLeftRight();
            }
            else if(GameController.instance.typeMove == TypeMove.Rotate)
            {
                Rotate();
            }
        }

        private void MoveUpDown()
        {
            if (this.gameObject.transform.position == new Vector3(0, 1, 0))
            {
                this.gameObject.transform.position -= new Vector3(0, 1, 0);
            }
            else if (this.gameObject.transform.position == new Vector3(0, -1, 0))
            {
                this.gameObject.transform.position += new Vector3(0, 1, 0);
            }
        }

        private void MoveLeftRight()
        {

            if(this.gameObject.transform.position == new Vector3(1, 0, 0))
            {
                this.gameObject.transform.position -= new Vector3(1, 0, 0);
            }
            else if (this.gameObject.transform.position == new Vector3(-1, 0, 0))
            {
                this.gameObject.transform.position += new Vector3(1, 0, 0);
            }
        }

        private void Rotate()
        {
            this.gameObject.transform.Rotate(new Vector3(0,0,10) * Time.deltaTime * _speed) ;
        }
    }
}