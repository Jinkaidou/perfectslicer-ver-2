using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GamePlay
{
    public class PointFruitController : MonoBehaviour
    {
        public int idPoint;
        private int countTime;
        private void Start()
        {
            this.gameObject.transform.GetComponent<CircleCollider2D>().isTrigger = true;
            //LeanTween.delayedCall(0.5f, () =>
            //{
            //    this.gameObject.transform.GetComponent<CircleCollider2D>().isTrigger = false;
            //});
        }

        private void Update()
        {
            countTime += 1;
            if(countTime == 20)
            {
                this.gameObject.transform.GetComponent<CircleCollider2D>().isTrigger = false;
            }
        }
    }
}