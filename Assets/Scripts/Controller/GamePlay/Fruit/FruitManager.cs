using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamePlay
{
    public class FruitManager : MonoBehaviour
    {
        public FruitInteractController[] fruit;

        public void InitData(int idFruit)
        {
            for(int i = 0; i< fruit.Length; i++)
            {
                if(idFruit == fruit[i].idFruit)
                {
                    Instantiate(fruit[i], this.gameObject.transform.position, this.gameObject.transform.rotation);
                }
            }
        }
    }
}

