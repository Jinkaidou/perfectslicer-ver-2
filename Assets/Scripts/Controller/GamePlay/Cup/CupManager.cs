﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataAccount;

namespace GamePlay
{
    public class CupManager : MonoBehaviour
    {
        public List <CupController> listCup = new List<CupController>();
        private int idCup;

        private void Start()
        {
            CreateCup();
        }

        public void CreateCup()
        {
            idCup = DataAccountPlayer.PlayerDataAccount.cupId;
            Debug.LogError(idCup + " id cốc");
            if(idCup == 0)
            {
                idCup = 3001;
            }
            for(int i = 0; i < listCup.Count; i++)
            {
                if(idCup == listCup[i].idCup)
                {
                   Instantiate(listCup[i], listCup[i].gameObject.transform.localPosition, this.gameObject.transform.rotation);
                }
            }
        }
    }
}