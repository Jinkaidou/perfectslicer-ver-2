﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JinLab.DataBase;
using Controller.LoadData;
using Core.Pool;
using DataAccount;
using AppsFlyerSDK;
using Lofelt.NiceVibrations;
using Base.Core.Sound;

namespace GamePlay
{
    public class GameController : SingletonMono<GameController>
    {

        [SerializeField] private FruitManager listfruit;
        [SerializeField] private KnifeController knife;
        [SerializeField] private ScaleController scale;
        [SerializeField] private GameObject barrier;

        [SerializeField] private PointFruitManager pointManager;
        private PointFruitController point;

        private int _mapID;

        private DataMoney _dataMoney;
        private MapCollectionData _mapData;

        private Transform pos1;
        private Transform pos2;
        //global Check
        public bool tranferInformation = true;
        public bool checkCreatePoint = true;
        public int idFruit;
        public TypeMove typeMove;

        private void Awake()
        {
            _mapID = DataAccountPlayer.PlayerProcessData.currentLevel;
            InitDataBase();
            InitLevelData();
            InitMoneyData();

            AppsFlyer.sendEvent($"play_level_{DataAccountPlayer.PlayerProcessData.currentLevel}", null);
            FirebaseManager.Instance.LogAnalyticsEvent($"play_level_{DataAccountPlayer.PlayerProcessData.currentLevel}");
        }


        private void InitDataBase()
        {
            _mapData = LoadResourceController.instance.LoadMapData();
            _dataMoney = LoadResourceController.instance.LoadDataMoney();
        }

        private void InitMoneyData()
        {
            var moneyAds = _dataMoney.baseMoneyAds;
            var levelAds = _dataMoney.levelUPAds;
            var bonusAds = _dataMoney.bonusAds;
            DataAccountPlayer.PlayerMoneyData.InitDataAds(moneyAds, levelAds, bonusAds);

            var money = _dataMoney.baseMoney;
            var level = _dataMoney.levelUP;
            var bonus = _dataMoney.bonus;
            DataAccountPlayer.PlayerMoneyData.InitDataStage(money, level, bonus);
        }

        private void InitLevelData()
        {
            var level = DataAccountPlayer.PlayerProcessData.currentLevel;
            if(level < _mapData.mapData.Count)
            {
                var mapData = _mapData.mapData[level];
                var idFruit = mapData.RandomFruit();
                listfruit.InitData(idFruit);
                typeMove = mapData.typeMove;
                knife.InitData(mapData.howManySlash);
            }
            else
            {
                var idIndex = Random.Range(0, _mapData.mapData.Count);
                var mapData = _mapData.mapData[idIndex];
                var idFruitHighLevel = mapData.RandomFruit();
                listfruit.InitData(idFruitHighLevel);
                typeMove = mapData.typeMove;
                knife.InitData(mapData.howManySlash);
            }

        }


    }
}