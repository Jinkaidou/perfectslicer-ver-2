﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using JinLab.DataBase;
using Controller.LoadData;

namespace GamePlay
{
    public class ScaleController : MonoBehaviour
    {

        [SerializeField] private Button cheatScale;

        private int _countStar;
        public List<float> massFruit = new List<float>();

        private WinLoseData _winLoseData;
        private void Awake()
        {
            _winLoseData = LoadResourceController.Instance.LoadWinLoseData();

            this.RegisterListener(EventID.CanSlash, (sender, param) =>
            {
                massFruit.Add((float)param * 8.5f);
            });

            this.RegisterListener(EventID.EndSlash, (sender, param) =>
            {
                CalculatePercent();
            });
        }

        private void CalculatePercent()
        {
            if(massFruit.Count > 0)
            for(int i = 0; i< massFruit.Count; i++)
            {
                var mass1 = massFruit[i];
                for (int j = 0; j < massFruit.Count; j++)
                {
                    var mass2 = massFruit[j];
                    CalculateStar(mass1, mass2);
                }
            }
            if(_countStar > 0)
            {
                this.PostEvent(EventID.WinGame,_countStar);
            }
        }

        private void CalculateStar(float a, float b)
        {
            if (a - b > _winLoseData.lose)
            {
                this.PostEvent(EventID.LoseGame);
                _countStar = 0;
                return;
            }
            if (a - b <= _winLoseData.oneStar)
            {
                _countStar = 1;
            }
            if( a- b <= _winLoseData.twoStar)
            {
                _countStar = 2;
            }
            if(a - b <= _winLoseData.threeStar)
            {
                _countStar = 3;
            }

        }

    }
}

