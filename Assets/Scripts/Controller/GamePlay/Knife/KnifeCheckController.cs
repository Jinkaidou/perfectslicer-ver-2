﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamePlay
{

    public class KnifeCheckController : MonoBehaviour
    {
        public bool check;
        public bool checkMove;
        private Vector3 _pos;
        private int _speed;

        private void Start()
        {
            check = true;
        }

        private void FixedUpdate()
        {
            if (checkMove)
            {
                _speed = 10;
                this.transform.localPosition += _pos * Time.deltaTime * _speed;
            }
        }

        public void InitPost(GameObject knifeA)
        {
            checkMove = true;
            var direction = knifeA.transform.localPosition;
            _pos = direction - transform.position;
            _pos = new Vector3(_pos.x, _pos.y, 0);
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("fruit"))
            {
                check = false;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("fruit"))
            {
                check = true;
            }
        }
    }
}