﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using CW.Common;
using Core.Pool;
using LinearAlgebra;
using DataAccount;
using Lofelt.NiceVibrations;
using Base.Core.Sound;
using DG.Tweening;

namespace GamePlay
{
    public class KnifeController : SingletonMono<KnifeController>
    {

        [SerializeField] private SpriteRenderer image;

        [SerializeField] private SpriteRenderer image1;

        [SerializeField] KnifeCheckController knife1;
        [SerializeField] KnifeCheckController knife2;



        private bool _checkIN = false;
        private bool _checkOUT = false;
        private bool _check = false;

        private bool startSlash = true;

        public Vector3 startPos;
        public Vector3 endPos;

        public Vector3 centerPoint;

        public Vector3 tf_Start;
        public Vector3 tf_End;
        public Transform look;

        private int _countSlash;
        private int _limitSlash;

        private void Start()
        {
            _checkIN = false;
            _checkOUT = false;

            image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
            image1.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
            startPos = new Vector3(0, 0, 0);


        }

        public void InitData(int limit)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
            var path = DataAccountPlayer.PlayerDataAccount.PathKnife();
            image.sprite = Resources.Load<Sprite>(path);
            image1.sprite = Resources.Load<Sprite>(path);
            _limitSlash = limit;
        }

        private void CalulateCenter()
        {
            var x = (startPos.x + endPos.x) / 2;
            var y = (startPos.y + endPos.y) / 2;
            centerPoint = new Vector2(x, y);
        }

        public virtual void OnEnable()
        {
            CwInputManager.OnFingerDown += HandleFingerDown;
            CwInputManager.OnFingerUp += HandleFingerUp;
            CwInputManager.OnFingerUpdate += HandleFingerUpdate;
        }

        public virtual void OnDisable()
        {
            CwInputManager.OnFingerDown -= HandleFingerDown;
            CwInputManager.OnFingerUp -= HandleFingerUp;
            CwInputManager.OnFingerUpdate -= HandleFingerUpdate;

        }

        public virtual void OnDestroy()
        {
            CwInputManager.OnFingerDown -= HandleFingerDown;
            CwInputManager.OnFingerUp -= HandleFingerUp;
            CwInputManager.OnFingerUpdate -= HandleFingerUpdate;
        }

        public virtual void HandleFingerDown(CwInputManager.Finger finger)
        {
            if (startSlash)
            {

                knife1.checkMove = false;

                image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
                image1.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
                startPos = Camera.main.ScreenToWorldPoint
                (new Vector3(Input.mousePosition.x, (Input.mousePosition.y), (transform.position.z - Camera.main.transform.position.z)));

                image1.gameObject.transform.localPosition = Camera.main.ScreenToWorldPoint
                (new Vector3(Input.mousePosition.x, (Input.mousePosition.y), (transform.position.z - Camera.main.transform.position.z)));

                tf_Start = Camera.main.ScreenToWorldPoint(new Vector3(finger.ScreenPosition.x, finger.ScreenPosition.y, Camera.main.nearClipPlane));
            }


        }

        public virtual void HandleFingerUp(CwInputManager.Finger finger)
        {
            if (startSlash)
            {
                image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
                endPos = Camera.main.ScreenToWorldPoint
                (new Vector3(Input.mousePosition.x, (Input.mousePosition.y), (transform.position.z - Camera.main.transform.position.z)));
                CalulateCenter();
                image1.color = new Color(image.color.r, image.color.g, image.color.b, 0f);

                tf_End = Camera.main.ScreenToWorldPoint(new Vector3(finger.ScreenPosition.x, finger.ScreenPosition.y, Camera.main.nearClipPlane));
                // CheckLeftOrRight(tf_Start.position - );

                knife1.InitPost(knife2.gameObject);

                RaycastHit2D[] hitss = Physics2D.RaycastAll(knife1.transform.localPosition,
                    (knife2.gameObject.transform.localPosition - knife1.transform.localPosition));
                _check = false;
                if (!_check)
                {
                    for (int i = 0; i < hitss.Length; i++)
                    {
                        if (hitss[i].transform.CompareTag("fruit"))
                        {
                            _check = true;
                            break;
                        }
                    }
                }

                DOVirtual.DelayedCall(0.1f, () =>
                {
                    knife1.checkMove = false;
                });

                if (knife1.check && knife2.check && _checkIN && _checkOUT && _check)
                {

                    this.PostEvent(EventID.HandOut);
                    this.PostEvent(EventID.CreatePoint);
                    _checkIN = false;
                    _checkOUT = false;


                    var isHaptic = DataAccountPlayer.PlayerSettings.VibrationOff;
                    var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
                    if (!isHaptic)
                    {
                        HapticPatterns.PlayPreset(HapticPatterns.PresetType.MediumImpact);
                    }
                    if (!isSound)
                    {
                        SoundManager.Instance.PlaySound(SoundType.knife_slash1);
                    }

                    _countSlash += 1;
                    if(_countSlash >= _limitSlash)
                    {
                        this.PostEvent(EventID.EndSlash);
                    }

                }
            }

        }

        public virtual void HandleFingerUpdate(CwInputManager.Finger finger)
        {
            this.gameObject.transform.localPosition = Camera.main.ScreenToWorldPoint
           (new Vector3(Input.mousePosition.x, (Input.mousePosition.y), (transform.position.z - Camera.main.transform.position.z)));
        }
    }
}