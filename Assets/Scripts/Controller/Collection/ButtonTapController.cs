using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Core;
using UI.LoadingScene;
using UnityEngine.UI;
using DataAccount;
using TMPro;
using Base.Core.Sound;

namespace GamePlay.UI.Collection
{
    public class ButtonTapController : MonoBehaviour
    {
        public CollectionController _collectionController;
        public ItemRewardManager _itemRewardManager;

        [SerializeField] private Button buttonCup;
        [SerializeField] private GameObject state1Cup;
        [SerializeField] private GameObject state2Cup;
        [SerializeField] private GameObject notiKnife;

        [SerializeField] private Button buttonKinfe;
        [SerializeField] private GameObject state1Knife;
        [SerializeField] private GameObject state2Knife;
        [SerializeField] private GameObject notiCup;

        private void Awake()
        {
            LisenterButton();
        }

        private void LisenterButton()
        {
            buttonCup.onClick.AddListener(OnClickButtonCup);
            buttonKinfe.onClick.AddListener(OnClickButtonKnife);
        }

        private void OnClickButtonCup()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
            state2Knife.SetActive(true);
            state2Cup.SetActive(false);
            _itemRewardManager.ChangeCupTab();
        }

        private void OnClickButtonKnife()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
            state2Cup.SetActive(true);
            state2Knife.SetActive(false);
            _itemRewardManager.ChangeKnifeTab();
        }
    }
}