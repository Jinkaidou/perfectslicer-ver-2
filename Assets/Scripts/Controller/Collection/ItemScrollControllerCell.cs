using System;
using System.Net.Mime;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using JinLab.DataBase;
using DataAccount;
using UnityEngine.UI;
using TMPro;
using System.Linq;

namespace GamePlay.UI.Collection
{
    class ItemScrollControllerCell : FancyCell<ItemScrollCell, ItemScrollContext>
    {
        public ItemRewardManager itemRewardManager;

        [Sirenix.OdinInspector.Title("Scroll")]
        public int m_ID;
        public int m_ItemID;
        public float currentPosition = 0;
        public bool currentSelection;
        public Animator m_Anim;
        public static readonly int Scroll = Animator.StringToHash("ItemSlide");

        [Sirenix.OdinInspector.Title("Visual")]
        public Image img_Item;
        public TextMeshProUGUI txt_Des;
        public Button btn_Ads;

        private void Awake()
        {
            itemRewardManager = FindAnyObjectByType<ItemRewardManager>();

            btn_Ads.onClick.AddListener(WatchAds);

            // this.RegisterListener(EventID.OnReloadItemScrollView, (sender, param) => SetupCell());
        }

        public void WatchAds()
        {
            itemRewardManager.OpenAdsClaimPopup(m_ItemID, itemRewardManager.collectionTabType);
        }

        public override void UpdateContent(ItemScrollCell itemData)
        {
            m_ID = itemData.m_ID;
            m_ItemID = itemData.m_ItemID;
            SetupCell();
            SetSelection(Context.SelectedIndex == m_ID);
        }

        [Sirenix.OdinInspector.Button]
        public void TestButton()
        {
            DataAccountPlayer.PlayerProcessData.dayCheckin = 1;
            DataAccountPlayer.SavePlayerProcessData();
        }

        [Sirenix.OdinInspector.Button]
        public void SetupCell()
        {
            CollectionData data = new CollectionData();

            if (itemRewardManager.collectionTabType == CollectionTabType.CUP)
            {
                var sprite = Resources.Load<Sprite>($"Sprite/CupItems/{m_ItemID.ToString()}");
                img_Item.sprite = sprite;
                data = itemRewardManager._listCollectionData.collectiveCup.Find(x => x.Id == m_ItemID);
            }
            else if (itemRewardManager.collectionTabType == CollectionTabType.KNIFE)
            {
                var sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{m_ItemID.ToString()}");
                img_Item.sprite = sprite;
                data = itemRewardManager._listCollectionData.collectiveKnife.Find(x => x.Id == m_ItemID);
            }


            if (data.typeUnlock == TypeCose.UnlockShop)
            {
                txt_Des.text = "Unlock from shop";
                btn_Ads.gameObject.SetActive(false);
            }
            else if (data.typeUnlock == TypeCose.UnlockFromCampaign)
            {
                if (DataAccountPlayer.PlayerProcessData.currentLevel >= data.cost)
                {
                    txt_Des.text = String.Empty;
                    btn_Ads.gameObject.SetActive(true);
                }
                else
                {
                    btn_Ads.gameObject.SetActive(false);
                    txt_Des.text = "Unlock at level " + data.cost;
                }
            }
            else if (data.typeUnlock == TypeCose.UnlockDailyCheckin)
            {
                if (DataAccountPlayer.PlayerProcessData.dayCheckin >= data.cost)
                {
                    txt_Des.text = String.Empty;
                    btn_Ads.gameObject.SetActive(true);
                }
                else
                {
                    btn_Ads.gameObject.SetActive(false);
                    txt_Des.text = "Unlock at day " + data.cost + " check in";
                }
            }
        }

        void SetSelection(bool selected)
        {
            if (currentSelection == selected)
            {
                return;
            }

            currentSelection = selected;

            SetupCell();
        }

        public override void UpdatePosition(float position)
        {
            currentPosition = position;

            if (m_Anim.isActiveAndEnabled)
            {
                m_Anim.Play(Scroll, -1, position);
            }

            m_Anim.speed = 0;
        }
    }
}