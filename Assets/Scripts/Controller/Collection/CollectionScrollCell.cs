using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using JinLab.DataBase;
using DataAccount;

namespace GamePlay.UI.Collection
{
    public class CollectionScrollCell : FancyCell<ItemCollectionCell, ItemCollectionContext>
    {
        public ItemRewardManager itemRewardManager;

        [Sirenix.OdinInspector.Title("Scroll")]
        public int m_ID;
        public float currentPosition = 0;
        public bool currentSelection;
        public Animator m_Anim;
        public static readonly int Scroll = Animator.StringToHash("Slide");

        [Sirenix.OdinInspector.Title("Index")]
        public ListCollectionData data;
        public List<ItemReward> itemRewards;
        public int startIndex;
        public int endIndex;

        private void Awake()
        {
            itemRewardManager = FindAnyObjectByType<ItemRewardManager>();
            this.RegisterListener(EventID.OnReloadCollectionScrollView, (sender, param) => SetupCell());
        }

        public override void UpdateContent(ItemCollectionCell itemData)
        {
            SetupCell();
            m_ID = itemData.m_ID;
            SetSelection(Context.SelectedIndex == m_ID);
        }

        [Sirenix.OdinInspector.Button]
        public void GetSelected()
        {
            Debug.Log("ID: " + DataAccountPlayer.PlayerCollectionData.selectedCup);
        }

        [Sirenix.OdinInspector.Button]
        public void SetupCell()
        {
            startIndex = m_ID * 12;
            endIndex = startIndex + 11;

            if (itemRewardManager.collectionTabType == CollectionTabType.CUP)
            {
                if (endIndex > data.collectiveCup.Count - 1)
                {
                    endIndex = data.collectiveCup.Count - 1;
                }
            }
            else if (itemRewardManager.collectionTabType == CollectionTabType.KNIFE)
            {
                if (endIndex > data.collectiveKnife.Count - 1)
                {
                    endIndex = data.collectiveKnife.Count - 1;
                }
            }

            int itemSetup = endIndex - startIndex + 1;
            int itemSetupIndex = 0;

            for (int i = 0; i < itemRewards.Count; i++)
            {
                if (itemSetupIndex < itemSetup)
                {
                    itemRewards[i].gameObject.SetActive(true);
                }
                else
                {
                    itemRewards[i].gameObject.SetActive(false);
                }
                itemSetupIndex++;
            }

            int itemReward = startIndex;

            if (itemRewardManager.collectionTabType == CollectionTabType.CUP)
            {
                for (int i = 0; i < itemRewards.Count; i++)
                {
                    if (itemReward <= data.collectiveCup.Count - 1)
                    {
                        itemRewards[i].collectionType = CollectionTabType.CUP;
                        itemRewards[i].itemId = data.collectiveCup[itemReward].Id;
                        var cupData = DataAccountPlayer.PlayerCollectionData.cupData.Find(x => x.id == itemRewards[i].itemId);
                        itemRewards[i].lockOn.SetActive(cupData == null);
                        itemRewards[i].selected.SetActive(itemRewards[i].itemId == DataAccountPlayer.PlayerCollectionData.selectedCup);

                        if (cupData != null)
                        {
                            itemRewards[i].adsIcon.SetActive(!cupData.isClaimAds);
                            itemRewards[i].noti.SetActive(!cupData.isClaimAds);
                        }
                        else
                        {
                            itemRewards[i].adsIcon.SetActive(false);
                            itemRewards[i].noti.SetActive(false);
                        }

                        var sprite = Resources.Load<Sprite>($"Sprite/CupItems/{itemRewards[i].itemId.ToString()}");
                        itemRewards[i].reward.sprite = sprite;
                        itemRewards[i].pathSource = $"Sprite/CupItems/{itemRewards[i].itemId.ToString()}";
                        itemRewards[i].reward.SetNativeSize();

                        itemReward++;
                    }
                }
            }
            else if (itemRewardManager.collectionTabType == CollectionTabType.KNIFE)
            {
                for (int i = 0; i < itemRewards.Count; i++)
                {
                    if (itemReward <= data.collectiveKnife.Count - 1)
                    {
                        itemRewards[i].collectionType = CollectionTabType.KNIFE;
                        itemRewards[i].itemId = data.collectiveKnife[itemReward].Id;
                        var knifeData = DataAccountPlayer.PlayerCollectionData.knifeData.Find(x => x.id == itemRewards[i].itemId);
                        itemRewards[i].lockOn.SetActive(knifeData == null);
                        itemRewards[i].selected.SetActive(itemRewards[i].itemId == DataAccountPlayer.PlayerCollectionData.selectedKnife);

                        if (knifeData != null)
                        {
                            itemRewards[i].adsIcon.SetActive(!knifeData.isClaimAds);
                            itemRewards[i].noti.SetActive(!knifeData.isClaimAds);
                        }
                        else
                        {
                            itemRewards[i].adsIcon.SetActive(false);
                            itemRewards[i].noti.SetActive(false);
                        }

                        var sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{itemRewards[i].itemId.ToString()}");
                        itemRewards[i].pathSource = $"Sprite/KnifeItems/{itemRewards[i].itemId.ToString()}";
                        itemRewards[i].reward.sprite = sprite;
                        itemRewards[i].reward.SetNativeSize();

                        itemReward++;
                    }
                }
            }
        }

        [Sirenix.OdinInspector.Button]
        public void CheckNUll()
        {
            for (int i = 0; i < DataAccountPlayer.PlayerCollectionData.cupData.Count; i++)
            {
                Debug.Log("ID: " + DataAccountPlayer.PlayerCollectionData.cupData[i].id);
            }
        }

        void SetSelection(bool selected)
        {
            if (currentSelection == selected)
            {
                return;
            }

            currentSelection = selected;

        }

        public override void UpdatePosition(float position)
        {
            currentPosition = position;

            if (m_Anim.isActiveAndEnabled)
            {
                m_Anim.Play(Scroll, -1, position);
            }

            m_Anim.speed = 0;
        }

        [Sirenix.OdinInspector.Button]
        public void GetIndex()
        {
            Debug.Log("Selected Index:" + Context.SelectedIndex);
        }
    }
}