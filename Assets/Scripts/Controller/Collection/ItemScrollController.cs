using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI.Extensions;
using UnityEngine.UI.Extensions.EasingCore;

namespace GamePlay.UI.Collection
{
    class ItemScrollController : FancyScrollView<ItemScrollCell, ItemScrollContext>
    {
        public ItemRewardManager itemRewardManager;

        [SerializeField] Scroller scroller = default;
        [SerializeField] GameObject cellPrefab = default;

        protected override GameObject CellPrefab => cellPrefab;

        public UnityEngine.UI.Button btn_Next;
        public UnityEngine.UI.Button btn_Prev;

        System.Action<int> onSelectionChanged;

        private void Start()
        {
            btn_Next.onClick.AddListener(SelectNextCell);
            btn_Prev.onClick.AddListener(SelectPrevCell);
        }

        protected override void Initialize()
        {
            base.Initialize();

            Context.OnCellClicked = SelectCell;

            scroller.OnValueChanged(UpdatePosition);
            scroller.OnSelectionChanged(UpdateSelection);
        }

        public void UpdateData(IList<ItemScrollCell> items)
        {
            UpdateContents(items);
            scroller.SetTotalCount(items.Count);
        }

        public void JumpTo(int index)
        {
            scroller.JumpTo(index);
        }

        public void ScrollTo(float position, float duration, Ease easing, System.Action onComplete = null)
        {
            scroller.ScrollTo(position, duration, easing, onComplete);
        }

        public void SelectNextCell()
        {
            SelectCell(Context.SelectedIndex + 1);
        }

        public void SelectPrevCell()
        {
            SelectCell(Context.SelectedIndex - 1);
        }

        public void SelectCell(int index)
        {
            UpdateSelection(index);
            ScrollTo(index, 0.35f, Ease.OutCubic);
        }

        public void UpdateSelection(int index)
        {
            Context.SelectedIndex = index;
            Refresh();

            onSelectionChanged?.Invoke(index);
        }

        public void OnSelectionChanged(System.Action<int> callback)
        {
            onSelectionChanged = callback;
        }
    }

    public class ItemScrollCell
    {
        public int m_ID;
        public int m_ItemID;

        public ItemScrollCell(int _id, int _itemID)
        {
            m_ID = _id;
            m_ItemID = _itemID;
        }
    }

    public class ItemScrollContext
    {
        public int SelectedIndex = -1;

        public System.Action<int> OnCellClicked;
    }
}
