using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI.Extensions;
using UnityEngine.UI.Extensions.EasingCore;
using JinLab.DataBase;
using DataAccount;
using Base.Core.Sound;

namespace GamePlay.UI.Collection
{
    class CollectionScrollController : FancyScrollView<ItemCollectionCell, ItemCollectionContext>
    {
        public ItemRewardManager itemRewardManager;

        [SerializeField] Scroller scroller = default;
        [SerializeField] GameObject cellPrefab = default;

        protected override GameObject CellPrefab => cellPrefab;

        public UnityEngine.UI.Button btn_Next;
        public UnityEngine.UI.Button btn_Prev;

        System.Action<int> onSelectionChanged;

        private void Start()
        {
            btn_Next.onClick.AddListener(SelectNextCell);
            btn_Prev.onClick.AddListener(SelectPrevCell);
        }

        protected override void Initialize()
        {
            base.Initialize();

            Context.OnCellClicked = SelectCell;

            scroller.OnValueChanged(UpdatePosition);
            scroller.OnSelectionChanged(UpdateSelection);
        }

        public void UpdateData(IList<ItemCollectionCell> items)
        {
            UpdateContents(items);
            scroller.SetTotalCount(items.Count);
        }

        public void JumpTo(int index)
        {
            scroller.JumpTo(index);
        }

        public void ScrollTo(float position, float duration, Ease easing, System.Action onComplete = null)
        {
            scroller.ScrollTo(position, duration, easing, onComplete);
        }

        public void SelectNextCell()
        {
            Debug.Log("Context.SelectedIndex: " + Context.SelectedIndex);
            playSound();
            SelectCell(Context.SelectedIndex + 1);
        }

        public void SelectPrevCell()
        {
            Debug.Log("Context.SelectedIndex: " + Context.SelectedIndex);
            playSound();
            SelectCell(Context.SelectedIndex - 1);
        }

        private void playSound()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
        }
        public void SelectCell(int index)
        {
            if (itemRewardManager.collectionTabType == CollectionTabType.CUP)
            {
                // if (index >= m_ListCollectionData.collectiveCup.Count)
                if (index > itemRewardManager.indexMax + 1)
                {
                    Debug.Log("MAX index: " + index);
                    index = 0;
                }
                else if (index < -1)
                {
                    Debug.Log("MIN index: " + index);
                    index = itemRewardManager.indexMax;
                }
            }
            else if (itemRewardManager.collectionTabType == CollectionTabType.KNIFE)
            {
                // if (index >= m_ListCollectionData.collectiveKnife.Count)
                if (index > itemRewardManager.indexMax + 1)
                {
                    Debug.Log("MAX index: " + index);
                    index = 0;
                }
                else if (index < -1)
                {
                    Debug.Log("MIN index: " + index);
                    index = itemRewardManager.indexMax;
                }
            }

            UpdateSelection(index);
            ScrollTo(index, 0.35f, Ease.OutCubic);
        }

        public void UpdateSelection(int index)
        {
            // if (Context.SelectedIndex == index)
            // {
            //     Debug.Log("INDEX: " + index);
            //     return;
            // }

            Context.SelectedIndex = index;
            Refresh();

            onSelectionChanged?.Invoke(index);
        }

        public void OnSelectionChanged(System.Action<int> callback)
        {
            onSelectionChanged = callback;
        }
    }

    public class ItemCollectionCell
    {
        public int m_ID;

        public ItemCollectionCell(int _id)
        {
            m_ID = _id;
        }
    }

    public class ItemCollectionContext
    {
        public int SelectedIndex = -1;

        public System.Action<int> OnCellClicked;
    }
}

