using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Core;
using UI.LoadingScene;
using UnityEngine.UI;
using DataAccount;
using TMPro;
using JinLab.DataBase;
using Controller.LoadData;
using Base.Core.Sound;

namespace GamePlay.UI.Collection
{
    public class CollectionController : MonoBehaviour
    {

        [SerializeField]private Button closeBtn;
        [SerializeField]private Button cheat;

        private void Awake()
        {
            closeBtn.onClick.AddListener(BackMainMenu);
            cheat.onClick.AddListener(CheatAllItem);
        }

        private void BackMainMenu()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
            GameManager.Instance.LoadSenceNew(SceneName.HomeScene);
        }

        private void CheatAllItem()
        {
            this.PostEvent(EventID.CheatAllItem);
        }
    }
}