﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using JinLab.DataBase;
using AppsFlyerSDK;
using Base.Core.Sound;
using DataAccount;
using Lofelt.NiceVibrations;
using UI.GamePlay;
// using JinLab.DataBase;

namespace GamePlay.UI.Collection
{
    public class RewardClaimPopup : MonoBehaviour
    {
        [Sirenix.OdinInspector.Title("AdsClaimPopup")]
        public GameObject go_AdsClaimPopup;
        public Button btn_NoThanks;
        public Button btn_ClaimAds;
        public Button btn_Claim;
        public Image img_Reward;
        public Image img_Result;
        public GameObject bublle;
        public TextMeshProUGUI txt_Result;

        [Sirenix.OdinInspector.Title("AdsClaimPopup_Result")]
        public GameObject go_Reward;
        public GameObject go_GiftBox;
        public GameObject go_Result;

        public ListCollectionData _listCollectionData;
        public ItemRewardManager itemRewardManager;

        public int CupOrKnife;

        private bool _checkPurchase;
        private bool _stillHaptic;

        [SerializeField] private GameObject title;
        [SerializeField] private GameObject case1;
        [SerializeField] private GameObject case2;
        [SerializeField] private AnimBoxController animBox;




        private void Awake()
        {
            itemRewardManager = FindAnyObjectByType<ItemRewardManager>();

            btn_NoThanks.onClick.AddListener(CloseAdsClaimPopup);
            btn_Claim.onClick.AddListener(CloseAdsClaimPopup);
            btn_ClaimAds.onClick.AddListener(WatchAdsReward);

            this.RegisterListener(EventID.OnShowReward, (sender, param) => ShowAdsReward());
        }

        private void PlaySound()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
        }

        [Sirenix.OdinInspector.Button]
        public void OpenAdsClaimPopup(int _id, CollectionTabType _type, bool checkPurchase)
        {
            title.SetActive(checkPurchase);
            case1.SetActive(checkPurchase);
            case2.SetActive(!checkPurchase);
            bublle.gameObject.SetActive(checkPurchase);
            img_Reward.gameObject.SetActive(checkPurchase);

            btn_NoThanks.gameObject.SetActive(checkPurchase);
            _checkPurchase = checkPurchase;

            go_AdsClaimPopup.SetActive(true);
            go_Reward.SetActive(true);
            go_GiftBox.SetActive(false);
            go_Result.SetActive(false);

            // tf_GiftIcon.DOShakePosition(1.5f, strength: 20, vibrato: 5000, randomness: 90, snapping: true, fadeOut: true, randomnessMode: ShakeRandomnessMode.Harmonic);

            if (_type == CollectionTabType.CUP)
            {
                img_Reward.sprite = Resources.Load<Sprite>($"Sprite/CupItems/{_id.ToString()}");
            }
            else if (_type == CollectionTabType.KNIFE)
            {
                img_Reward.sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{_id.ToString()}");
            }
            Debug.Log("TTTTTTTTTTTTT");
            DOVirtual.DelayedCall(1.5f, () =>
                btn_NoThanks.gameObject.SetActive(checkPurchase)
            );
        }

        public void CloseAdsClaimPopup()
        {
            PlaySound();
            btn_NoThanks.gameObject.SetActive(false);
            go_AdsClaimPopup.SetActive(false);
        }

        public void ShowAdsReward()
        {
            CollectionData result = new CollectionData();
            animBox.OpenBox();

            if (itemRewardManager is null)
            {

                if(CupOrKnife == 1)
                {
                    result = _listCollectionData.collectiveCup.Find(x => x.Id == AdsManager.Instance.rewardId);
                    img_Result.sprite = Resources.Load<Sprite>($"Sprite/CupItems/{AdsManager.Instance.rewardId.ToString()}");
                }
                else if(CupOrKnife == 2)
                {
                    result = _listCollectionData.collectiveKnife.Find(x => x.Id == AdsManager.Instance.rewardId);
                    img_Result.sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{AdsManager.Instance.rewardId.ToString()}");
                }
                AppsFlyer.sendEvent($"item_{AdsManager.Instance.rewardId}_unlock", null);
                FirebaseManager.Instance.LogAnalyticsEvent($"item_{AdsManager.Instance.rewardId}_unlock");
                txt_Result.text = result.name;

                go_Reward.SetActive(false);
               // go_GiftBox.SetActive(true);
                go_Result.SetActive(false);

                DOVirtual.DelayedCall(2f, () =>
                {
                    var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
                    if (!isSound)
                    {
                        SoundManager.Instance.PlaySound(SoundType.open_gift);
                    }
                    go_Reward.SetActive(false);
                    go_GiftBox.SetActive(false);
                    go_Result.SetActive(true);
                });

            }
            else
            {
                if (itemRewardManager.collectionTabType == CollectionTabType.CUP)
                {
                    result = _listCollectionData.collectiveCup.Find(x => x.Id == AdsManager.Instance.rewardId);
                    img_Result.sprite = Resources.Load<Sprite>($"Sprite/CupItems/{AdsManager.Instance.rewardId.ToString()}");
                }
                else if (itemRewardManager.collectionTabType == CollectionTabType.KNIFE)
                {
                    result = _listCollectionData.collectiveKnife.Find(x => x.Id == AdsManager.Instance.rewardId);
                    img_Result.sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{AdsManager.Instance.rewardId.ToString()}");
                }
                txt_Result.text = result.name;

                go_Reward.SetActive(false);
                //go_GiftBox.SetActive(true);
                go_Result.SetActive(false);

                DOVirtual.DelayedCall(2f, () =>
                {
                    go_Reward.SetActive(false);
                    go_GiftBox.SetActive(false);
                    go_Result.SetActive(true);
                    _stillHaptic = false;
                    var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
                    if (!isSound)
                    {
                        SoundManager.Instance.PlaySound(SoundType.open_gift);
                    }
                });
            }

        }

        public void WatchAdsReward()
        {
            PlaySound();
            if (_checkPurchase)
            {
                AdsManager.Instance.ShowAdsRewards(RewardType.SHOP_ITEM_REWARD);
            }
            else
            {
                ShowAdsReward();
            }
        }
    }
}