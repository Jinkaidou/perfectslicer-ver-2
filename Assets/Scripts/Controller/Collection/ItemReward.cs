using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Core;
using UI.LoadingScene;
using UnityEngine.UI;
using DataAccount;
using TMPro;

namespace GamePlay.UI.Collection
{
    public class ItemReward : MonoBehaviour
    {
        public CollectionTabType collectionType;
        public int itemId;
        public Image reward;
        public GameObject selected;
        public GameObject lockOn;
        public GameObject adsIcon;
        public GameObject noti;
        public Button btn_Owner;
        public string pathSource;

        private ItemRewardManager itemRewardManager;

        private void Awake()
        {
            InitData();
            LisenterButton();
            itemRewardManager = FindObjectOfType<ItemRewardManager>();
            this.RegisterListener(EventID.CheatAllItem, (sender, param) =>
            {
                if(collectionType == CollectionTabType.CUP)
                {
                    TestClaimCup_TRUE();
                }
                else if(collectionType == CollectionTabType.KNIFE)
                {
                    TestClaimKnife();
                }
            });

        }

        private void LisenterButton()
        {
            btn_Owner.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            PlayerCollectionItem item = new PlayerCollectionItem();
            if (collectionType == CollectionTabType.CUP)
            {
                item = DataAccountPlayer.PlayerCollectionData.cupData.Find(x => x.id == itemId);
            }
            else if (collectionType == CollectionTabType.KNIFE)
            {
                item = DataAccountPlayer.PlayerCollectionData.knifeData.Find(x => x.id == itemId);
            }

            if (item == null)
            {
                itemRewardManager.itemScrollID = itemId;
                itemRewardManager.OpenItemScrollView();
            }
            else
            {
                if (!item.isClaimAds)
                {
                    AdsManager.Instance.rewardId = itemId;
                    AdsManager.Instance.collectionType = collectionType;
                    itemRewardManager.OpenAdsClaimPopup(itemId, collectionType);
                }
                else
                {
                    if (collectionType == CollectionTabType.CUP)
                    {
                        itemRewardManager.OnSetItem(itemId);
                        DataAccountPlayer.PlayerCollectionData.selectedCup = itemId;
                        Debug.LogError(itemId + " thay doi ID");
                        DataAccountPlayer.PlayerDataAccount.SetCupID(itemId, pathSource);
                       // DataAccountPlayer.PlayerDataAccount.SetCupID(itemId, reward.sprite.name);
                    }
                    else if (collectionType == CollectionTabType.KNIFE)
                    {
                        itemRewardManager.OnSetItem(itemId);
                        DataAccountPlayer.PlayerCollectionData.selectedKnife = itemId;
                        DataAccountPlayer.PlayerDataAccount.SetKnifeID(itemId, pathSource);
                    }
                    DataAccountPlayer.SavePlayerCollectionData();
                    this.PostEvent(EventID.OnReloadCollectionScrollView, itemId);
                }
            }
        }

        private void InitData()
        {
            // var resultT = Resources.Load<Sprite>($"Sprite/CupItems/{itemId.ToString()}");
            // reward.sprite = Resources.Load<Sprite>("/Sprite/CupItems/" + itemId.ToString() + ".png");
            // reward.sprite = resultT;
            // reward.SetNativeSize();
        }

        [Sirenix.OdinInspector.Button]
        public void TestClaimCup_TRUE()
        {
            var cup = new PlayerCollectionCup(itemId, true);
            var cupList = new PlayerCollectionCupList();
            cupList.Add(cup);
            // DataAccountPlayer.PlayerCollectionData.ClaimCup(cup);
            DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);

            this.PostEvent(EventID.OnReloadCollectionScrollView, itemId);
        }

        [Sirenix.OdinInspector.Button]
        public void TestClaimCup_FALSE()
        {
            var cup = new PlayerCollectionCup(itemId);
            var cupList = new PlayerCollectionCupList();
            cupList.Add(cup);
            // DataAccountPlayer.PlayerCollectionData.ClaimCup(cup);
            DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);

            this.PostEvent(EventID.OnReloadCollectionScrollView, itemId);
        }

        [Sirenix.OdinInspector.Button]
        public void TestClaimKnife()
        {
            var knife = new PlayerCollectionKnife(itemId);
            var knifeList = new PlayerCollectionKnifeList();
            knifeList.Add(knife);
            // DataAccountPlayer.PlayerCollectionData.ClaimCup(cup);
            DataAccountPlayer.PlayerCollectionData.ClaimKnife(knifeList);

            this.PostEvent(EventID.OnReloadCollectionScrollView, itemId);
        }
    }
}