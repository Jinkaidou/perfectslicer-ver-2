using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Core;
using UI.LoadingScene;
using UnityEngine.UI;
using DataAccount;
using TMPro;
using JinLab.DataBase;
using Controller.LoadData;
using DG.Tweening;
using System.Diagnostics;
using Cysharp.Threading.Tasks;
using Base.Core.Sound;

namespace GamePlay.UI.Collection
{
    public class ItemRewardManager : MonoBehaviour
    {
        [Sirenix.OdinInspector.Title("Item Scroller")]
        public int itemScrollID;
        public GameObject go_ItemScrollView;
        public Button btn_CloseItemScroll;

        [SerializeField] ItemScrollController itemScrollView = default;

        [Sirenix.OdinInspector.Title("CollectionScrollController")]
        public CollectionTabType collectionTabType;
        public ListCollectionData _listCollectionData;
        [SerializeField] CollectionScrollController scrollView = default;
        public int indexMax;

        [Sirenix.OdinInspector.Title("AdsClaimPopup")]
        public RewardClaimPopup go_AdsClaimPopup;
        public GameObject go_PrefabAdsClaimPopup;
        // public Button btn_NoThanks;
        // public Button btn_ClaimAds;
        // public Button btn_Claim;
        // public Image img_Reward;
        // public Image img_Result;
        // public TextMeshProUGUI txt_Result;

        // [Sirenix.OdinInspector.Title("AdsClaimPopup_Result")]
        // public GameObject go_Reward;
        // public GameObject go_GiftBox;
        // public GameObject go_Result;

        [Sirenix.OdinInspector.Title("Item Reward")]
        public Image img_RewardDisplay;
        public TextMeshProUGUI txt_RewardDisplay;

        private void Awake()
        {
            Application.targetFrameRate = 60;
            InitCollectionData();
            // btn_NoThanks.onClick.AddListener(CloseAdsClaimPopup);
            // btn_Claim.onClick.AddListener(CloseAdsClaimPopup);
            // btn_ClaimAds.onClick.AddListener(WatchAdsReward);
            btn_CloseItemScroll.onClick.AddListener(CloseItemScrollView);

            // this.RegisterListener(EventID.OnShowReward, (sender, param) => ShowAdsReward());
            this.RegisterListener(EventID.OnReloadCollectionScrollView, (sender, param) => OnSetItem((int)param));
        }

        private void OnEnable()
        {
            collectionTabType = CollectionTabType.CUP;
            SetupCollectionScrollView();
            // SetupItemScrollView();
        }

        public void InitCollectionData()
        {
            if (!ES3.KeyExists(DataAccountPlayerConstants.PlayerCollectionData))
            {
                var cup = new PlayerCollectionCup(3001, true);
                var cupList = new PlayerCollectionCupList();
                cupList.Add(cup);

                var knife = new PlayerCollectionKnife(4001, true);
                var knifeList = new PlayerCollectionKnifeList();
                knifeList.Add(knife);

                DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);
                DataAccountPlayer.PlayerCollectionData.ClaimKnife(knifeList);
                DataAccountPlayer.PlayerCollectionData.page = 0;
                DataAccountPlayer.PlayerCollectionData.selectedCup = 3001;
                DataAccountPlayer.PlayerCollectionData.selectedKnife = 4001;
                DataAccountPlayer.SavePlayerCollectionData();

                // var playerCollectionData = new PlayerCollectionData();
                // playerCollectionData.InitCollectionData(0, 3001, 4001, cupList, knifeList);
                this.PostEvent(EventID.OnReloadCollectionScrollView, 3001);
            }
        }

        [Sirenix.OdinInspector.Button]
        public void TestClaimCup_FALSE()
        {
            var cup = new PlayerCollectionCup(3002);
            var cupList = new PlayerCollectionCupList();
            cupList.Add(cup);
            // DataAccountPlayer.PlayerCollectionData.ClaimCup(cup);
            DataAccountPlayer.PlayerCollectionData.ClaimCup(cupList);

            this.PostEvent(EventID.OnReloadCollectionScrollView, 3002);
        }

        [Sirenix.OdinInspector.Button]
        public void OpenItemScrollView()
        {
            go_ItemScrollView.SetActive(true);
            SetupItemScrollView();
        }

        public void ShowAdsInter()
        {
            AdsManager.Instance.ShowAdsInter();
        }

        [Sirenix.OdinInspector.Button]
        public void CloseItemScrollView()
        {
            PlaySound();
            go_ItemScrollView.SetActive(false);
        }

        private void PlaySound()
        {
            var isSound = DataAccountPlayer.PlayerSettings.SoundOff;
            if (!isSound)
            {
                SoundManager.Instance.PlaySound(SoundType.button_click);
            }
        }

        public void SetupItemScrollView()
        {
            List<CollectionData> data;

            if (collectionTabType == CollectionTabType.CUP)
            {
                data = new List<CollectionData>(_listCollectionData.collectiveCup);
                List<CollectionData> remove = new List<CollectionData>();
                for (int i = 0; i < data.Count; i++)
                {
                    var exits = DataAccountPlayer.PlayerCollectionData.cupData.Find(x => (x.id == data[i].Id && x.isClaimAds));
                    if (exits != null)
                    {
                        remove.Add(data[i]);
                    }
                }
                data.RemoveAll(x => remove.Contains(x));
            }
            else if (collectionTabType == CollectionTabType.KNIFE)
            {
                data = new List<CollectionData>(_listCollectionData.collectiveKnife);
                List<CollectionData> remove = new List<CollectionData>();
                for (int i = 0; i < data.Count; i++)
                {
                    var exits = DataAccountPlayer.PlayerCollectionData.knifeData.Find(x => (x.id == data[i].Id && x.isClaimAds));
                    if (exits != null)
                    {
                        remove.Add(data[i]);
                    }
                }
                data.RemoveAll(x => remove.Contains(x));
            }
            else
            {
                return;
            }

            // DataAccountPlayer.PlayerCollectionData.cupData

            var items = Enumerable.Range(0, data.Count - 1)
                                        .Select(i => new ItemScrollCell(i, data[i].Id))
                                        .ToList();
            itemScrollView.UpdateData(items);
            var item = items.Find(x => x.m_ItemID == itemScrollID);
            itemScrollView.UpdateSelection(item.m_ID);
            itemScrollView.JumpTo(item.m_ID);
            OnCollectionScrollSelectionChanged(item.m_ID);
        }

        public void SetupCollectionScrollView()
        {
            int i = 0;
            int endIndex = 12 * (i + 1) - 1;
            int startIndex = endIndex - 11;

            List<CollectionData> data = new List<CollectionData>();

            if (collectionTabType == CollectionTabType.CUP)
            {
                data = _listCollectionData.collectiveCup;
            }
            else if (collectionTabType == CollectionTabType.KNIFE)
            {
                data = _listCollectionData.collectiveKnife;
            }

            while (startIndex < data.Count)
            {
                i++;
                endIndex = 12 * (i + 1) - 1;
                startIndex = endIndex - 11;
            }

            indexMax = i - 1;

            // log

            var items = Enumerable.Range(0, indexMax + 1)
                            .Select(i => new ItemCollectionCell(i))
                            .ToArray();
            scrollView.UpdateData(items);
            scrollView.UpdateSelection(DataAccountPlayer.PlayerCollectionData.page);
            scrollView.JumpTo(DataAccountPlayer.PlayerCollectionData.page);
            OnCollectionScrollSelectionChanged(DataAccountPlayer.PlayerCollectionData.page);
        }

        void OnCollectionScrollSelectionChanged(int index)
        {
            // UnityEngine.Debug.Log("Index: " + DataAccountPlayer.PlayerCollectionData.page);
        }

        void OnItemScrollSelectionChanged(int index)
        {
            // UnityEngine.Debug.Log("Index: " + DataAccountPlayer.PlayerCollectionData.page);
        }

        public async UniTask OpenAdsClaimPopup(int _id, CollectionTabType _type)
        {
            if (go_AdsClaimPopup == null)
            {
                go_AdsClaimPopup = GameObject.Instantiate(go_PrefabAdsClaimPopup, parent: this.transform).GetComponent<RewardClaimPopup>();
                await UniTask.WaitUntil(() => go_AdsClaimPopup.gameObject.activeSelf);
                go_AdsClaimPopup.OpenAdsClaimPopup(_id, _type, true);
            }
            else
            {
                go_AdsClaimPopup.gameObject.SetActive(true);
                await UniTask.WaitUntil(() => go_AdsClaimPopup.gameObject.activeSelf);
                go_AdsClaimPopup.OpenAdsClaimPopup(_id, _type,true);
            }
            // go_AdsClaimPopup.SetActive(true);
            // go_Reward.SetActive(true);
            // go_GiftBox.SetActive(false);
            // go_Result.SetActive(false);

            // // tf_GiftIcon.DOShakePosition(1.5f, strength: 20, vibrato: 5000, randomness: 90, snapping: true, fadeOut: true, randomnessMode: ShakeRandomnessMode.Harmonic);

            // if (_type == CollectionTabType.CUP)
            // {
            //     img_Reward.sprite = Resources.Load<Sprite>($"Sprite/CupItems/{_id.ToString()}");
            // }
            // else if (_type == CollectionTabType.KNIFE)
            // {
            //     img_Reward.sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{_id.ToString()}");
            // }

            // DOVirtual.DelayedCall(1.5f, () =>
            //     btn_NoThanks.gameObject.SetActive(true)
            // );
        }

        // public void ShowAdsReward()
        // {
        //     UnityEngine.Debug.Log("AAAAAAAAAA");

        //     CollectionData result = new CollectionData();
        //     if (collectionTabType == CollectionTabType.CUP)
        //     {
        //         result = _listCollectionData.collectiveCup.Find(x => x.Id == AdsManager.Instance.rewardId);
        //         img_Result.sprite = Resources.Load<Sprite>($"Sprite/CupItems/{AdsManager.Instance.rewardId.ToString()}");
        //     }
        //     else if (collectionTabType == CollectionTabType.KNIFE)
        //     {
        //         result = _listCollectionData.collectiveKnife.Find(x => x.Id == AdsManager.Instance.rewardId);
        //         img_Result.sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{AdsManager.Instance.rewardId.ToString()}");
        //     }


        //     txt_Result.text = result.name;

        //     go_Reward.SetActive(false);
        //     go_GiftBox.SetActive(true);
        //     go_Result.SetActive(false);

        //     DOVirtual.DelayedCall(1.5f, () =>
        //     {
        //         go_Reward.SetActive(false);
        //         go_GiftBox.SetActive(false);
        //         go_Result.SetActive(true);
        //     });
        // }

        // public void CloseAdsClaimPopup()
        // {
        //     btn_NoThanks.gameObject.SetActive(false);
        //     go_AdsClaimPopup.SetActive(false);
        // }

        public void WatchAdsReward()
        {
            AdsManager.Instance.ShowAdsRewards(RewardType.SHOP_ITEM_REWARD);
        }

        public void ChangeCupTab()
        {
            collectionTabType = CollectionTabType.CUP;
            SetupCollectionScrollView();
            OnSetItem(DataAccountPlayer.PlayerCollectionData.selectedCup);
            this.PostEvent(EventID.OnReloadCollectionScrollView);
        }

        public void ChangeKnifeTab()
        {
            collectionTabType = CollectionTabType.KNIFE;
            SetupCollectionScrollView();
            OnSetItem(DataAccountPlayer.PlayerCollectionData.selectedKnife);
            this.PostEvent(EventID.OnReloadCollectionScrollView);
        }

        public void OnSetItem(int _id)
        {
            UnityEngine.Debug.Log("ID = " + _id);
            CollectionData item = new CollectionData();
            if (collectionTabType == CollectionTabType.CUP)
            {
                item = _listCollectionData.collectiveCup.Find(x => x.Id == _id);
                if (DataAccountPlayer.PlayerCollectionData.cupData.Find(x => x.id == _id).isClaimAds)
                {
                    UnityEngine.Debug.Log("AAAAAAAAAAAAAAAA");
                    img_RewardDisplay.sprite = Resources.Load<Sprite>($"Sprite/CupItems/{_id.ToString()}");
                    txt_RewardDisplay.text = item.name;
                }
            }
            else if (collectionTabType == CollectionTabType.KNIFE)
            {
                item = _listCollectionData.collectiveKnife.Find(x => x.Id == _id);
                if (DataAccountPlayer.PlayerCollectionData.knifeData.Find(x => x.id == _id).isClaimAds)
                {
                    UnityEngine.Debug.Log("BBBBBBBBBBBBBBBB");
                    img_RewardDisplay.sprite = Resources.Load<Sprite>($"Sprite/KnifeItems/{_id.ToString()}");
                    txt_RewardDisplay.text = item.name;
                }
            }
        }
    }

    public enum CollectionTabType
    {
        CUP,
        KNIFE,
    }
}