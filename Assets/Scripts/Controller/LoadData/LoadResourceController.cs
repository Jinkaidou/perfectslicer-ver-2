﻿using System.Collections.Generic;
using System.IO;
using Base.Core.Sound;
using UnityEngine;
using JinLab.DataBase;
using Object = UnityEngine.Object;

namespace Controller.LoadData
{
    public class LoadResourceController : SingletonMono<LoadResourceController>
    {
        private Dictionary<string, Object> _resourceCache = new Dictionary<string, Object>();

        #region LoadMethod

        private T Load<T>(string path, string fileName) where T : Object
        {
            var fullPath = Path.Combine(path, fileName);
            if (_resourceCache.ContainsKey(fullPath) is false)
            {
                _resourceCache.Add(fullPath, TryToLoad<T>(path, fileName));
            }

            return _resourceCache[fullPath] as T;
        }

        private static T TryToLoad<T>(string path, string fileName) where T : Object
        {
            var fullPath = Path.Combine(path, fileName);
            var result = Resources.Load<T>(fullPath);
            return result;
        }

        #endregion

        #region Public Load Method


        public GameObject LoadPanel(string panelName)
        {
            return Load<GameObject>(ResourcesFolderPath.UiFolder, panelName);
        }

        public AudioClip LoadAudioClip(SoundType soundType)
        {
            return Load<AudioClip>(ResourcesFolderPath.SoundFolder, soundType.ToString());
        }


        #endregion

        #region LoadDataAsset
        // cách load data từ scriptable Object
        //public FoodTypeCollection LoadFoodDataCollection()
        //{
        //    var path = string.Format(ResourcesFolderPath.DataFolder , ResourcesFolderPath.DataFolderFood );
        //    return Load<FoodTypeCollection(tên class của scriptable Object)>(path, "FoodData"(tên của scriptable Object));
        //}

        public DataDifficultyCollection LoadDataDifficultyCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderDifficulty);
            return Load < DataDifficultyCollection>(path, "DifficultyData");
        }

        public LevelData LoadDataLevelData()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderPhaseData);
            return Load<LevelData>(path, "PhaseData");
        }

        public DataMoney LoadDataMoney()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderMoneyData);
            return Load<DataMoney>(path, "MoneyData");
        }

        public ListCollectionData LoadDataCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderListCollectionData);
            return Load<ListCollectionData>(path, "ListCollectionData");
        }

        public PriceCollectionData LoadDataPrice()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderPriceCollectionData);
            return Load<PriceCollectionData>(path, "PriceCollectionData");
        }

        public BonusMoneyData LoadDataBonusMoney()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderBonusMoneyData);
            return Load<BonusMoneyData>(path, "BonusMoneyData");
        }

        public MapCollectionData LoadMapData()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderMapData);
            return Load<MapCollectionData>(path, "MapData");
        }

        public WinLoseData LoadWinLoseData()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderWinLoseData);
            return Load<WinLoseData>(path, "WinLoseData");
        }

        #endregion
    }
}
