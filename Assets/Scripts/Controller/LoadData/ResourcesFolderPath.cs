﻿
namespace Controller.LoadData
{
    public static class ResourcesFolderPath
    {
        //Data

        // tên của biến dùng để trỏ đến DATA = // tên folder của Data đó
        public const string DataFolder = "DATA/{0}/";
        public const string DataFolderDifficulty = "Difficulty";
        public const string DataFolderPhaseData = "PhaseData";
        public const string DataFolderMoneyData = "MoneyData";

        public const string DataFolderListCollectionData = "ListCollectionData";
        public const string DataFolderBonusMoneyData = "BonusMoneyData";
        public const string DataFolderPriceCollectionData = "PriceCollectionData";
        public const string DataFolderMapData = "MapData";
        public const string DataFolderWinLoseData = "WinLoseData";


        public const string UiFolder = "UI/";
        public const string SoundFolder = "SOUNDS/";
        public const string IconFolder = "ICONS";
        public const string HeroFolder = "HeroIO";
    }
}

